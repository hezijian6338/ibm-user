import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false
Vue.prototype.$Url = 'https://fanyi-api.baidu.com'
console.log(Vue.prototype)
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
