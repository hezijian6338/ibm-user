var baiduToken // 百度touken
import MD5 from '@/common/md5.js'
// 翻译
export const translation = function(v){
	// 有道翻译,暂时废除
	// return new Promise(function (resolve, reject){
	// 	v = v.replace(/_/g, ' ')
	// 	uni.request({
	// 		url:'https://ltalk.foxcall.cn/fanyi/translate',
	// 		header: {
	// 			'content-type': 'application/x-www-form-urlencoded'
	// 		},
	// 		method:'GET',
	// 		data: {
	// 			doctype: 'json',
	// 			type: 'AUTO',
	// 			i: v
	// 		},
	// 		success:(res)=>{
	// 			if (res.statusCode === 200) {
	// 				let msg = {
	// 					text: res.data.translateResult[0][0].tgt
	// 				}
	// 				resolve(msg)
	// 				// this.sendMsg(msg, 'text&img')
	// 			} else {
	// 				uni.showToast({
	// 					title: '翻译错误，请重试',
	// 					icon: 'none',
	// 					duration: 3000
	// 				})
	// 				reject()
	// 			}
	// 		},
	// 		fail:(res)=>{
	// 			//console.log("admin获取access_token失败")
	// 		}
	// 	})
	// })
	
	// 有道翻译,暂时废除
	return new Promise(function (resolve, reject){
		v = v.replace(/_/g, ' ')
		let appid = '20200117000376221';
		let key = 'HMeLhKB2PiKRZgLpQdvw';
		let salt = (new Date).getTime();
		let sign = MD5(appid + v + salt + key)
		uni.request({
			url:'https://ltalk.foxcall.cn/baidu/api/trans/vip/translate',
			data: {
				q: v,
				appid: appid,
				salt: salt,
				from: 'en',
				to: 'zh',
				sign: sign
			},
			header: {
				// 'Access-Control-Allow-Origin': '*',
				'content-type': 'application/x-www-form-urlencoded'
			},
			method:'GET',
			success:(res)=>{
				if (res.statusCode === 200) {
					let msg = {
						text: res.data.trans_result[0].dst
					}
					resolve(msg)
				} else {
					uni.showToast({
						title: '翻译错误，请重试',
						icon: 'none',
						duration: 3000
					})
					reject()
				}
			},
			fail:(res)=>{
				//console.log("admin获取access_token失败")
			}
		})
	})
}
// 百度识图
export const getBDImages = function(base64){
	return new Promise(function (resolve, reject){
		uni.request({
			url:'https://aip.baidubce.com/rpc/2.0/ai_custom/v1/classification/garbage_c?access_token=' + baiduToken,
			header: {
				'content-type': 'application/json'
			},
			method:'POST',
			data: {
				image: base64,
				top_num: 5
				// access_token: '24.0e473e13ca1a4f0faf35db8f29ff7b94.2592000.1580614329.282335-18139558#/'
			},
			success:(data)=>{
				uni.hideLoading()
				if (data.statusCode === 200) {
					resolve(data)
				} else {
					uni.showToast({
						title: '识别图片失败，请尝试其他图片',
						icon: 'none',
						duration: 3000
					})
				}
			},
			fail:(data)=>{
				uni.showToast({
					title: 'fail' + data.error_msg,
					icon: 'none',
					duration: 3000
				})
			}
		})
	})
}
// 百度touken接口
export const getBDaccessToken = function() {
	uni.request({
		url:'https://ltalk.foxcall.cn/oauth/2.0/token',
		header: {
			'content-type': 'application/x-www-form-urlencoded'
		},
		method:'GET',
		data: {
			grant_type: 'client_credentials',
			client_id: 'yVahty2n1G9MSgLdoUp3B5Gd',
			client_secret: 'YGVX1dsded3sX0U9bghPbNG92xsZx4YR'
		},
		success:(res)=>{
			if (res.statusCode === 200) {
				baiduToken = res.data.access_token
			} else {
				uni.showToast({
					title: '获取百度接口失败，请重试' + res.statusCode,
					icon: 'none',
					duration: 3000
				})
			}
		},
		fail:(res)=>{
			//console.log("admin获取access_token失败")
		}
	})
}
// 转base64
export const urlTobase64 = function(url) {
	return new Promise(function (resolve, reject){
		uni.request({
			url: url,
			method:'GET',
			responseType: 'arraybuffer',
			success: ress => {
				resolve(ress.data)
			}
		})
	})
}
//获取微信accesstoken
export const getHouDuanAccess = function(){
	return new Promise(function (resolve, reject){
		uni.request({
			url:'https://talk.foxcall.cn/apis/saas/wechat/access/token',
			method:'GET',
			success:(res)=>{
				if(res.data.code==200){
					resolve(res)
				}else{
					uni.showToast({
						icon:'none',
						title:'从后台获取access_token相关参数失败'
					})
				}
			},
			fail:(res)=>{
				//console.log("admin获取access_token失败")
			}
		})
	})
}
// 初始获取session
export const get_session = function(url, base64_auth){
	return new Promise(function (resolve, reject){
		uni.request({
			url: url,
			header: {
				Authorization: 'Basic ' + base64_auth
			},
			method: 'POST',
			success: res => {
				resolve(res.data.session_id)
			},
			fail: res => {
				uni.showToast({
					title: '获取session失败,请返回首页重新进入',
					icon: 'none',
					duration: 3000
				})
			}
		})
	})
}
// 发送消息
export const sendMsg = function(url, base64_auth_user, text) {
	return new Promise(function (resolve, reject){
		uni.request({
			url,
			header: {
				Authorization: 'Basic ' + base64_auth_user
			},
			method: 'POST',
			data: {
				input: {
					text
				}
			},
			success: res => {
				uni.hideLoading();
				if (res.data.code == 400 || res.data.code == 404 || res.data.code == 403) {
					uni.showToast({
						icon: 'none',
						title: 'session过期,请重新刷新浏览器',
						duration: 3000
					});
				} else {
					resolve(res.data)
				}
			},
			fail: res => {
				uni.hideLoading();
				uni.showToast({
					icon: 'none',
					title: '出错，请联系管理员'
				})
			}
		})
	})
}
