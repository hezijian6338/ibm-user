// 返回"yyyy-mm-dd"
function appendZero (obj) {
   if (obj < 10) {
      return '0' + obj
    } else {
      return obj
    }
}
export const dataFormat_no = function(input, pattern = ""){ // 在参数列表中 通过 pattern="" 来指定形参默认值，防止报错
	let dt = new Date();
	if(input){
		dt = new Date(input);
	}
	// 获取年月日
	let y = dt.getFullYear();
	let m = appendZero((dt.getMonth() + 1).toString());
	let d = appendZero(dt.getDate().toString());
	// 如果 传递进来的字符串类型，转为小写之后，等于 yyyy-mm-dd，那么就返回 年-月-日
	// 否则，就返回  年-月-日 时：分：秒
	return `${y}`+`-`+`${m}`+`-`+`${d}`;
}