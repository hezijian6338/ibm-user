var __pageFrameStartTime__ = Date.now();
var __webviewId__;
var __wxAppCode__ = {};
var __WXML_GLOBAL__ = {
  entrys: {},
  defines: {},
  modules: {},
  ops: [],
  wxs_nf_init: undefined,
  total_ops: 0
};
var $gwx;

/*v0.5vv_20190312_syb_scopedata*/window.__wcc_version__='v0.5vv_20190312_syb_scopedata';window.__wcc_version_info__={"customComponents":true,"fixZeroRpx":true,"propValueDeepCopy":false};
var $gwxc
var $gaic={}
$gwx=function(path,global){
if(typeof global === 'undefined') global={};if(typeof __WXML_GLOBAL__ === 'undefined') {__WXML_GLOBAL__={};
}__WXML_GLOBAL__.modules = __WXML_GLOBAL__.modules || {};
function _(a,b){if(typeof(b)!='undefined')a.children.push(b);}
function _v(k){if(typeof(k)!='undefined')return {tag:'virtual','wxKey':k,children:[]};return {tag:'virtual',children:[]};}
function _n(tag){$gwxc++;if($gwxc>=16000){throw 'Dom limit exceeded, please check if there\'s any mistake you\'ve made.'};return {tag:'wx-'+tag,attr:{},children:[],n:[],raw:{},generics:{}}}
function _p(a,b){b&&a.properities.push(b);}
function _s(scope,env,key){return typeof(scope[key])!='undefined'?scope[key]:env[key]}
function _wp(m){console.warn("WXMLRT_$gwx:"+m)}
function _wl(tname,prefix){_wp(prefix+':-1:-1:-1: Template `' + tname + '` is being called recursively, will be stop.')}
$gwn=console.warn;
$gwl=console.log;
function $gwh()
{
function x()
{
}
x.prototype = 
{
hn: function( obj, all )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any1=false,any2=false;
for(var x in obj)
{
any1=any1|x==='__value__';
any2=any2|x==='__wxspec__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any1 && any2 && ( all || obj.__wxspec__ !== 'm' || this.hn(obj.__value__) === 'h' ) ? "h" : "n";
}
return "n";
},
nh: function( obj, special )
{
return { __value__: obj, __wxspec__: special ? special : true }
},
rv: function( obj )
{
return this.hn(obj,true)==='n'?obj:this.rv(obj.__value__);
},
hm: function( obj )
{
if( typeof(obj) == 'object' )
{
var cnt=0;
var any1=false,any2=false;
for(var x in obj)
{
any1=any1|x==='__value__';
any2=any2|x==='__wxspec__';
cnt++;
if(cnt>2)break;
}
return cnt == 2 && any1 && any2 && (obj.__wxspec__ === 'm' || this.hm(obj.__value__) );
}
return false;
}
}
return new x;
}
wh=$gwh();
function $gstack(s){
var tmp=s.split('\n '+' '+' '+' ');
for(var i=0;i<tmp.length;++i){
if(0==i) continue;
if(")"===tmp[i][tmp[i].length-1])
tmp[i]=tmp[i].replace(/\s\(.*\)$/,"");
else
tmp[i]="at anonymous function";
}
return tmp.join('\n '+' '+' '+' ');
}
function $gwrt( should_pass_type_info )
{
function ArithmeticEv( ops, e, s, g, o )
{
var _f = false;
var rop = ops[0][1];
var _a,_b,_c,_d, _aa, _bb;
switch( rop )
{
case '?:':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o, _f ) : rev( ops[3], e, s, g, o, _f );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '&&':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? rev( ops[2], e, s, g, o, _f ) : wh.rv( _a );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '||':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && ( wh.hn(_a) === 'h' );
_d = wh.rv( _a ) ? wh.rv(_a) : rev( ops[2], e, s, g, o, _f );
_d = _c && wh.hn( _d ) === 'n' ? wh.nh( _d, 'c' ) : _d;
return _d;
break;
case '+':
case '*':
case '/':
case '%':
case '|':
case '^':
case '&':
case '===':
case '==':
case '!=':
case '!==':
case '>=':
case '<=':
case '>':
case '<':
case '<<':
case '>>':
_a = rev( ops[1], e, s, g, o, _f );
_b = rev( ops[2], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
switch( rop )
{
case '+':
_d = wh.rv( _a ) + wh.rv( _b );
break;
case '*':
_d = wh.rv( _a ) * wh.rv( _b );
break;
case '/':
_d = wh.rv( _a ) / wh.rv( _b );
break;
case '%':
_d = wh.rv( _a ) % wh.rv( _b );
break;
case '|':
_d = wh.rv( _a ) | wh.rv( _b );
break;
case '^':
_d = wh.rv( _a ) ^ wh.rv( _b );
break;
case '&':
_d = wh.rv( _a ) & wh.rv( _b );
break;
case '===':
_d = wh.rv( _a ) === wh.rv( _b );
break;
case '==':
_d = wh.rv( _a ) == wh.rv( _b );
break;
case '!=':
_d = wh.rv( _a ) != wh.rv( _b );
break;
case '!==':
_d = wh.rv( _a ) !== wh.rv( _b );
break;
case '>=':
_d = wh.rv( _a ) >= wh.rv( _b );
break;
case '<=':
_d = wh.rv( _a ) <= wh.rv( _b );
break;
case '>':
_d = wh.rv( _a ) > wh.rv( _b );
break;
case '<':
_d = wh.rv( _a ) < wh.rv( _b );
break;
case '<<':
_d = wh.rv( _a ) << wh.rv( _b );
break;
case '>>':
_d = wh.rv( _a ) >> wh.rv( _b );
break;
default:
break;
}
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '-':
_a = ops.length === 3 ? rev( ops[1], e, s, g, o, _f ) : 0;
_b = ops.length === 3 ? rev( ops[2], e, s, g, o, _f ) : rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) === 'h' || wh.hn( _b ) === 'h');
_d = _c ? wh.rv( _a ) - wh.rv( _b ) : _a - _b;
return _c ? wh.nh( _d, "c" ) : _d;
break;
case '!':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = !wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
case '~':
_a = rev( ops[1], e, s, g, o, _f );
_c = should_pass_type_info && (wh.hn( _a ) == 'h');
_d = ~wh.rv(_a);
return _c ? wh.nh( _d, "c" ) : _d;
default:
$gwn('unrecognized op' + rop );
}
}
function rev( ops, e, s, g, o, newap )
{
var op = ops[0];
var _f = false;
if ( typeof newap !== "undefined" ) o.ap = newap;
if( typeof(op)==='object' )
{
var vop=op[0];
var _a, _aa, _b, _bb, _c, _d, _s, _e, _ta, _tb, _td;
switch(vop)
{
case 2:
return ArithmeticEv(ops,e,s,g,o);
break;
case 4: 
return rev( ops[1], e, s, g, o, _f );
break;
case 5: 
switch( ops.length )
{
case 2: 
_a = rev( ops[1],e,s,g,o,_f );
return should_pass_type_info?[_a]:[wh.rv(_a)];
return [_a];
break;
case 1: 
return [];
break;
default:
_a = rev( ops[1],e,s,g,o,_f );
_b = rev( ops[2],e,s,g,o,_f );
_a.push( 
should_pass_type_info ?
_b :
wh.rv( _b )
);
return _a;
break;
}
break;
case 6:
_a = rev(ops[1],e,s,g,o);
var ap = o.ap;
_ta = wh.hn(_a)==='h';
_aa = _ta ? wh.rv(_a) : _a;
o.is_affected |= _ta;
if( should_pass_type_info )
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return _ta ? wh.nh(undefined, 'e') : undefined;
}
_b = rev(ops[2],e,s,g,o,_f);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.ap = ap;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' || 
_bb === "__proto__" || _bb === "prototype" || _bb === "caller" ) 
{
return (_ta || _tb) ? wh.nh(undefined, 'e') : undefined;
}
_d = _aa[_bb];
if ( typeof _d === 'function' && !ap ) _d = undefined;
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return (_ta || _tb) ? (_td ? _d : wh.nh(_d, 'e')) : _d;
}
else
{
if( _aa===null || typeof(_aa) === 'undefined' )
{
return undefined;
}
_b = rev(ops[2],e,s,g,o,_f);
_tb = wh.hn(_b) === 'h';
_bb = _tb ? wh.rv(_b) : _b;
o.ap = ap;
o.is_affected |= _tb;
if( _bb===null || typeof(_bb) === 'undefined' || 
_bb === "__proto__" || _bb === "prototype" || _bb === "caller" ) 
{
return undefined;
}
_d = _aa[_bb];
if ( typeof _d === 'function' && !ap ) _d = undefined;
_td = wh.hn(_d)==='h';
o.is_affected |= _td;
return _td ? wh.rv(_d) : _d;
}
case 7: 
switch(ops[1][0])
{
case 11:
o.is_affected |= wh.hn(g)==='h';
return g;
case 3:
_s = wh.rv( s );
_e = wh.rv( e );
_b = ops[1][1];
if (g && g.f && g.f.hasOwnProperty(_b) )
{
_a = g.f;
o.ap = true;
}
else
{
_a = _s && _s.hasOwnProperty(_b) ? 
s : (_e && _e.hasOwnProperty(_b) ? e : undefined );
}
if( should_pass_type_info )
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
_d = _ta && !_td ? wh.nh(_d,'e') : _d;
return _d;
}
}
else
{
if( _a )
{
_ta = wh.hn(_a) === 'h';
_aa = _ta ? wh.rv( _a ) : _a;
_d = _aa[_b];
_td = wh.hn(_d) === 'h';
o.is_affected |= _ta || _td;
return wh.rv(_d);
}
}
return undefined;
}
break;
case 8: 
_a = {};
_a[ops[1]] = rev(ops[2],e,s,g,o,_f);
return _a;
break;
case 9: 
_a = rev(ops[1],e,s,g,o,_f);
_b = rev(ops[2],e,s,g,o,_f);
function merge( _a, _b, _ow )
{
var ka, _bbk;
_ta = wh.hn(_a)==='h';
_tb = wh.hn(_b)==='h';
_aa = wh.rv(_a);
_bb = wh.rv(_b);
for(var k in _bb)
{
if ( _ow || !_aa.hasOwnProperty(k) )
{
_aa[k] = should_pass_type_info ? (_tb ? wh.nh(_bb[k],'e') : _bb[k]) : wh.rv(_bb[k]);
}
}
return _a;
}
var _c = _a
var _ow = true
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
_a = _b
_b = _c
_ow = false
}
if ( typeof(ops[1][0]) === "object" && ops[1][0][0] === 10 ) {
var _r = {}
return merge( merge( _r, _a, _ow ), _b, _ow );
}
else
return merge( _a, _b, _ow );
break;
case 10:
_a = rev(ops[1],e,s,g,o,_f);
_a = should_pass_type_info ? _a : wh.rv( _a );
return _a ;
break;
case 12:
var _r;
_a = rev(ops[1],e,s,g,o);
if ( !o.ap )
{
return should_pass_type_info && wh.hn(_a)==='h' ? wh.nh( _r, 'f' ) : _r;
}
var ap = o.ap;
_b = rev(ops[2],e,s,g,o,_f);
o.ap = ap;
_ta = wh.hn(_a)==='h';
_tb = _ca(_b);
_aa = wh.rv(_a);	
_bb = wh.rv(_b); snap_bb=$gdc(_bb,"nv_");
try{
_r = typeof _aa === "function" ? $gdc(_aa.apply(null, snap_bb)) : undefined;
} catch (e){
e.message = e.message.replace(/nv_/g,"");
e.stack = e.stack.substring(0,e.stack.indexOf("\n", e.stack.lastIndexOf("at nv_")));
e.stack = e.stack.replace(/\snv_/g," "); 
e.stack = $gstack(e.stack);	
if(g.debugInfo)
{
e.stack += "\n "+" "+" "+" at "+g.debugInfo[0]+":"+g.debugInfo[1]+":"+g.debugInfo[2];
console.error(e);
}
_r = undefined;
}
return should_pass_type_info && (_tb || _ta) ? wh.nh( _r, 'f' ) : _r;
}
}
else
{
if( op === 3 || op === 1) return ops[1];
else if( op === 11 ) 
{
var _a='';
for( var i = 1 ; i < ops.length ; i++ )
{
var xp = wh.rv(rev(ops[i],e,s,g,o,_f));
_a += typeof(xp) === 'undefined' ? '' : xp;
}
return _a;
}
}
}
function wrapper( ops, e, s, g, o, newap )
{
if( ops[0] == '11182016' )
{
g.debugInfo = ops[2];
return rev( ops[1], e, s, g, o, newap );
}
else
{
g.debugInfo = null;
return rev( ops, e, s, g, o, newap );
}
}
return wrapper;
}
gra=$gwrt(true); 
grb=$gwrt(false); 
function TestTest( expr, ops, e,s,g, expect_a, expect_b, expect_affected )
{
{
var o = {is_affected:false};
var a = gra( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_a )
|| o.is_affected != expect_affected )
{
console.warn( "A. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_a ) + ", " + expect_affected + " is expected" );
}
}
{
var o = {is_affected:false};
var a = grb( ops, e,s,g, o );
if( JSON.stringify(a) != JSON.stringify( expect_b )
|| o.is_affected != expect_affected )
{
console.warn( "B. " + expr + " get result " + JSON.stringify(a) + ", " + o.is_affected + ", but " + JSON.stringify( expect_b ) + ", " + expect_affected + " is expected" );
}
}
}

function wfor( to_iter, func, env, _s, global, father, itemname, indexname, keyname )
{
var _n = wh.hn( to_iter ) === 'n'; 
var scope = wh.rv( _s ); 
var has_old_item = scope.hasOwnProperty(itemname);
var has_old_index = scope.hasOwnProperty(indexname);
var old_item = scope[itemname];
var old_index = scope[indexname];
var full = Object.prototype.toString.call(wh.rv(to_iter));
var type = full[8]; 
if( type === 'N' && full[10] === 'l' ) type = 'X'; 
var _y;
if( _n )
{
if( type === 'A' ) 
{
var r_iter_item;
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = _n ? i : wh.nh(i, 'h');
r_iter_item = wh.rv(to_iter[i]);
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y = _v(key);
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' ) 
{
var i = 0;
var r_iter_item;
for( var k in to_iter )
{
scope[itemname] = to_iter[k];
scope[indexname] = _n ? k : wh.nh(k, 'h');
r_iter_item = wh.rv(to_iter[k]);
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y = _v(key);
_(father,_y);
func( env,scope,_y,global );
i++;
}
}
else if( type === 'S' ) 
{
for( var i = 0 ; i < to_iter.length ; i++ )
{
scope[itemname] = to_iter[i];
scope[indexname] = _n ? i : wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env,scope,_y,global );
}
}
else if( type === 'N' ) 
{
for( var i = 0 ; i < to_iter ; i++ )
{
scope[itemname] = i;
scope[indexname] = _n ? i : wh.nh(i, 'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
else
{
var r_to_iter = wh.rv(to_iter);
var r_iter_item, iter_item;
if( type === 'A' ) 
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = r_to_iter[i];
iter_item = wh.hn(iter_item)==='n' ? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item
scope[indexname] = _n ? i : wh.nh(i, 'h');
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y = _v(key);
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'O' ) 
{
var i=0;
for( var k in r_to_iter )
{
iter_item = r_to_iter[k];
iter_item = wh.hn(iter_item)==='n'? wh.nh(iter_item,'h') : iter_item;
r_iter_item = wh.rv( iter_item );
scope[itemname] = iter_item;
scope[indexname] = _n ? k : wh.nh(k, 'h');
var key = keyname && r_iter_item ? (keyname==="*this" ? r_iter_item : wh.rv(r_iter_item[keyname])) : undefined;
_y=_v(key);
_(father,_y);
func( env, scope, _y, global );
i++
}
}
else if( type === 'S' ) 
{
for( var i = 0 ; i < r_to_iter.length ; i++ )
{
iter_item = wh.nh(r_to_iter[i],'h');
scope[itemname] = iter_item;
scope[indexname] = _n ? i : wh.nh(i, 'h');
_y = _v( to_iter[i] + i );
_(father,_y);
func( env, scope, _y, global );
}
}
else if( type === 'N' ) 
{
for( var i = 0 ; i < r_to_iter ; i++ )
{
iter_item = wh.nh(i,'h');
scope[itemname] = iter_item;
scope[indexname]= _n ? i : wh.nh(i,'h');
_y = _v( i );
_(father,_y);
func(env,scope,_y,global);
}
}
else
{
}
}
if(has_old_item)
{
scope[itemname]=old_item;
}
else
{
delete scope[itemname];
}
if(has_old_index)
{
scope[indexname]=old_index;
}
else
{
delete scope[indexname];
}
}

function _ca(o)
{ 
if ( wh.hn(o) == 'h' ) return true;
if ( typeof o !== "object" ) return false;
for(var i in o){ 
if ( o.hasOwnProperty(i) ){
if (_ca(o[i])) return true;
}
}
return false;
}
function _da( node, attrname, opindex, raw, o )
{
var isaffected = false;
var value = $gdc( raw, "", 2 );
if ( o.ap && value && value.constructor===Function ) 
{
attrname = "$wxs:" + attrname; 
node.attr["$gdc"] = $gdc;
}
if ( o.is_affected || _ca(raw) ) 
{
node.n.push( attrname );
node.raw[attrname] = raw;
}
node.attr[attrname] = value;
}
function _r( node, attrname, opindex, env, scope, global ) 
{
global.opindex=opindex;
var o = {}, _env;
var a = grb( z[opindex], env, scope, global, o );
_da( node, attrname, opindex, a, o );
}
function _rz( z, node, attrname, opindex, env, scope, global ) 
{
global.opindex=opindex;
var o = {}, _env;
var a = grb( z[opindex], env, scope, global, o );
_da( node, attrname, opindex, a, o );
}
function _o( opindex, env, scope, global )
{
global.opindex=opindex;
var nothing = {};
var r = grb( z[opindex], env, scope, global, nothing );
return (r&&r.constructor===Function) ? undefined : r;
}
function _oz( z, opindex, env, scope, global )
{
global.opindex=opindex;
var nothing = {};
var r = grb( z[opindex], env, scope, global, nothing );
return (r&&r.constructor===Function) ? undefined : r;
}
function _1( opindex, env, scope, global, o )
{
var o = o || {};
global.opindex=opindex;
return gra( z[opindex], env, scope, global, o );
}
function _1z( z, opindex, env, scope, global, o )
{
var o = o || {};
global.opindex=opindex;
return gra( z[opindex], env, scope, global, o );
}
function _2( opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var o = {};
var to_iter = _1( opindex, env, scope, global );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}
function _2z( z, opindex, func, env, scope, global, father, itemname, indexname, keyname )
{
var o = {};
var to_iter = _1z( z, opindex, env, scope, global );
wfor( to_iter, func, env, scope, global, father, itemname, indexname, keyname );
}


function _m(tag,attrs,generics,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(base+attrs[i+1]<0)
{
tmp.attr[attrs[i]]=true;
}
else
{
_r(tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];
}
}
for(var i=0;i<generics.length;i+=2)
{
if(base+generics[i+1]<0)
{
tmp.generics[generics[i]]="";
}
else
{
var $t=grb(z[base+generics[i+1]],env,scope,global);
if ($t!="") $t="wx-"+$t;
tmp.generics[generics[i]]=$t;
if(base===0)base=generics[i+1];
}
}
return tmp;
}
function _mz(z,tag,attrs,generics,env,scope,global)
{
var tmp=_n(tag);
var base=0;
for(var i = 0 ; i < attrs.length ; i+=2 )
{
if(base+attrs[i+1]<0)
{
tmp.attr[attrs[i]]=true;
}
else
{
_rz(z, tmp,attrs[i],base+attrs[i+1],env,scope,global);
if(base===0)base=attrs[i+1];
}
}
for(var i=0;i<generics.length;i+=2)
{
if(base+generics[i+1]<0)
{
tmp.generics[generics[i]]="";
}
else
{
var $t=grb(z[base+generics[i+1]],env,scope,global);
if ($t!="") $t="wx-"+$t;
tmp.generics[generics[i]]=$t;
if(base===0)base=generics[i+1];
}
}
return tmp;
}

var nf_init=function(){
if(typeof __WXML_GLOBAL__==="undefined"||undefined===__WXML_GLOBAL__.wxs_nf_init){
nf_init_Object();nf_init_Function();nf_init_Array();nf_init_String();nf_init_Boolean();nf_init_Number();nf_init_Math();nf_init_Date();nf_init_RegExp();
}
if(typeof __WXML_GLOBAL__!=="undefined") __WXML_GLOBAL__.wxs_nf_init=true;
};
var nf_init_Object=function(){
Object.defineProperty(Object.prototype,"nv_constructor",{writable:true,value:"Object"})
Object.defineProperty(Object.prototype,"nv_toString",{writable:true,value:function(){return "[object Object]"}})
}
var nf_init_Function=function(){
Object.defineProperty(Function.prototype,"nv_constructor",{writable:true,value:"Function"})
Object.defineProperty(Function.prototype,"nv_length",{get:function(){return this.length;},set:function(){}});
Object.defineProperty(Function.prototype,"nv_toString",{writable:true,value:function(){return "[function Function]"}})
}
var nf_init_Array=function(){
Object.defineProperty(Array.prototype,"nv_toString",{writable:true,value:function(){return this.nv_join();}})
Object.defineProperty(Array.prototype,"nv_join",{writable:true,value:function(s){
s=undefined==s?',':s;
var r="";
for(var i=0;i<this.length;++i){
if(0!=i) r+=s;
if(null==this[i]||undefined==this[i]) r+='';	
else if(typeof this[i]=='function') r+=this[i].nv_toString();
else if(typeof this[i]=='object'&&this[i].nv_constructor==="Array") r+=this[i].nv_join();
else r+=this[i].toString();
}
return r;
}})
Object.defineProperty(Array.prototype,"nv_constructor",{writable:true,value:"Array"})
Object.defineProperty(Array.prototype,"nv_concat",{writable:true,value:Array.prototype.concat})
Object.defineProperty(Array.prototype,"nv_pop",{writable:true,value:Array.prototype.pop})
Object.defineProperty(Array.prototype,"nv_push",{writable:true,value:Array.prototype.push})
Object.defineProperty(Array.prototype,"nv_reverse",{writable:true,value:Array.prototype.reverse})
Object.defineProperty(Array.prototype,"nv_shift",{writable:true,value:Array.prototype.shift})
Object.defineProperty(Array.prototype,"nv_slice",{writable:true,value:Array.prototype.slice})
Object.defineProperty(Array.prototype,"nv_sort",{writable:true,value:Array.prototype.sort})
Object.defineProperty(Array.prototype,"nv_splice",{writable:true,value:Array.prototype.splice})
Object.defineProperty(Array.prototype,"nv_unshift",{writable:true,value:Array.prototype.unshift})
Object.defineProperty(Array.prototype,"nv_indexOf",{writable:true,value:Array.prototype.indexOf})
Object.defineProperty(Array.prototype,"nv_lastIndexOf",{writable:true,value:Array.prototype.lastIndexOf})
Object.defineProperty(Array.prototype,"nv_every",{writable:true,value:Array.prototype.every})
Object.defineProperty(Array.prototype,"nv_some",{writable:true,value:Array.prototype.some})
Object.defineProperty(Array.prototype,"nv_forEach",{writable:true,value:Array.prototype.forEach})
Object.defineProperty(Array.prototype,"nv_map",{writable:true,value:Array.prototype.map})
Object.defineProperty(Array.prototype,"nv_filter",{writable:true,value:Array.prototype.filter})
Object.defineProperty(Array.prototype,"nv_reduce",{writable:true,value:Array.prototype.reduce})
Object.defineProperty(Array.prototype,"nv_reduceRight",{writable:true,value:Array.prototype.reduceRight})
Object.defineProperty(Array.prototype,"nv_length",{get:function(){return this.length;},set:function(value){this.length=value;}});
}
var nf_init_String=function(){
Object.defineProperty(String.prototype,"nv_constructor",{writable:true,value:"String"})
Object.defineProperty(String.prototype,"nv_toString",{writable:true,value:String.prototype.toString})
Object.defineProperty(String.prototype,"nv_valueOf",{writable:true,value:String.prototype.valueOf})
Object.defineProperty(String.prototype,"nv_charAt",{writable:true,value:String.prototype.charAt})
Object.defineProperty(String.prototype,"nv_charCodeAt",{writable:true,value:String.prototype.charCodeAt})
Object.defineProperty(String.prototype,"nv_concat",{writable:true,value:String.prototype.concat})
Object.defineProperty(String.prototype,"nv_indexOf",{writable:true,value:String.prototype.indexOf})
Object.defineProperty(String.prototype,"nv_lastIndexOf",{writable:true,value:String.prototype.lastIndexOf})
Object.defineProperty(String.prototype,"nv_localeCompare",{writable:true,value:String.prototype.localeCompare})
Object.defineProperty(String.prototype,"nv_match",{writable:true,value:String.prototype.match})
Object.defineProperty(String.prototype,"nv_replace",{writable:true,value:String.prototype.replace})
Object.defineProperty(String.prototype,"nv_search",{writable:true,value:String.prototype.search})
Object.defineProperty(String.prototype,"nv_slice",{writable:true,value:String.prototype.slice})
Object.defineProperty(String.prototype,"nv_split",{writable:true,value:String.prototype.split})
Object.defineProperty(String.prototype,"nv_substring",{writable:true,value:String.prototype.substring})
Object.defineProperty(String.prototype,"nv_toLowerCase",{writable:true,value:String.prototype.toLowerCase})
Object.defineProperty(String.prototype,"nv_toLocaleLowerCase",{writable:true,value:String.prototype.toLocaleLowerCase})
Object.defineProperty(String.prototype,"nv_toUpperCase",{writable:true,value:String.prototype.toUpperCase})
Object.defineProperty(String.prototype,"nv_toLocaleUpperCase",{writable:true,value:String.prototype.toLocaleUpperCase})
Object.defineProperty(String.prototype,"nv_trim",{writable:true,value:String.prototype.trim})
Object.defineProperty(String.prototype,"nv_length",{get:function(){return this.length;},set:function(value){this.length=value;}});
}
var nf_init_Boolean=function(){
Object.defineProperty(Boolean.prototype,"nv_constructor",{writable:true,value:"Boolean"})
Object.defineProperty(Boolean.prototype,"nv_toString",{writable:true,value:Boolean.prototype.toString})
Object.defineProperty(Boolean.prototype,"nv_valueOf",{writable:true,value:Boolean.prototype.valueOf})
}
var nf_init_Number=function(){
Object.defineProperty(Number,"nv_MAX_VALUE",{writable:false,value:Number.MAX_VALUE})
Object.defineProperty(Number,"nv_MIN_VALUE",{writable:false,value:Number.MIN_VALUE})
Object.defineProperty(Number,"nv_NEGATIVE_INFINITY",{writable:false,value:Number.NEGATIVE_INFINITY})
Object.defineProperty(Number,"nv_POSITIVE_INFINITY",{writable:false,value:Number.POSITIVE_INFINITY})
Object.defineProperty(Number.prototype,"nv_constructor",{writable:true,value:"Number"})
Object.defineProperty(Number.prototype,"nv_toString",{writable:true,value:Number.prototype.toString})
Object.defineProperty(Number.prototype,"nv_toLocaleString",{writable:true,value:Number.prototype.toLocaleString})
Object.defineProperty(Number.prototype,"nv_valueOf",{writable:true,value:Number.prototype.valueOf})
Object.defineProperty(Number.prototype,"nv_toFixed",{writable:true,value:Number.prototype.toFixed})
Object.defineProperty(Number.prototype,"nv_toExponential",{writable:true,value:Number.prototype.toExponential})
Object.defineProperty(Number.prototype,"nv_toPrecision",{writable:true,value:Number.prototype.toPrecision})
}
var nf_init_Math=function(){
Object.defineProperty(Math,"nv_E",{writable:false,value:Math.E})
Object.defineProperty(Math,"nv_LN10",{writable:false,value:Math.LN10})
Object.defineProperty(Math,"nv_LN2",{writable:false,value:Math.LN2})
Object.defineProperty(Math,"nv_LOG2E",{writable:false,value:Math.LOG2E})
Object.defineProperty(Math,"nv_LOG10E",{writable:false,value:Math.LOG10E})
Object.defineProperty(Math,"nv_PI",{writable:false,value:Math.PI})
Object.defineProperty(Math,"nv_SQRT1_2",{writable:false,value:Math.SQRT1_2})
Object.defineProperty(Math,"nv_SQRT2",{writable:false,value:Math.SQRT2})
Object.defineProperty(Math,"nv_abs",{writable:false,value:Math.abs})
Object.defineProperty(Math,"nv_acos",{writable:false,value:Math.acos})
Object.defineProperty(Math,"nv_asin",{writable:false,value:Math.asin})
Object.defineProperty(Math,"nv_atan",{writable:false,value:Math.atan})
Object.defineProperty(Math,"nv_atan2",{writable:false,value:Math.atan2})
Object.defineProperty(Math,"nv_ceil",{writable:false,value:Math.ceil})
Object.defineProperty(Math,"nv_cos",{writable:false,value:Math.cos})
Object.defineProperty(Math,"nv_exp",{writable:false,value:Math.exp})
Object.defineProperty(Math,"nv_floor",{writable:false,value:Math.floor})
Object.defineProperty(Math,"nv_log",{writable:false,value:Math.log})
Object.defineProperty(Math,"nv_max",{writable:false,value:Math.max})
Object.defineProperty(Math,"nv_min",{writable:false,value:Math.min})
Object.defineProperty(Math,"nv_pow",{writable:false,value:Math.pow})
Object.defineProperty(Math,"nv_random",{writable:false,value:Math.random})
Object.defineProperty(Math,"nv_round",{writable:false,value:Math.round})
Object.defineProperty(Math,"nv_sin",{writable:false,value:Math.sin})
Object.defineProperty(Math,"nv_sqrt",{writable:false,value:Math.sqrt})
Object.defineProperty(Math,"nv_tan",{writable:false,value:Math.tan})
}
var nf_init_Date=function(){
Object.defineProperty(Date.prototype,"nv_constructor",{writable:true,value:"Date"})
Object.defineProperty(Date,"nv_parse",{writable:true,value:Date.parse})
Object.defineProperty(Date,"nv_UTC",{writable:true,value:Date.UTC})
Object.defineProperty(Date,"nv_now",{writable:true,value:Date.now})
Object.defineProperty(Date.prototype,"nv_toString",{writable:true,value:Date.prototype.toString})
Object.defineProperty(Date.prototype,"nv_toDateString",{writable:true,value:Date.prototype.toDateString})
Object.defineProperty(Date.prototype,"nv_toTimeString",{writable:true,value:Date.prototype.toTimeString})
Object.defineProperty(Date.prototype,"nv_toLocaleString",{writable:true,value:Date.prototype.toLocaleString})
Object.defineProperty(Date.prototype,"nv_toLocaleDateString",{writable:true,value:Date.prototype.toLocaleDateString})
Object.defineProperty(Date.prototype,"nv_toLocaleTimeString",{writable:true,value:Date.prototype.toLocaleTimeString})
Object.defineProperty(Date.prototype,"nv_valueOf",{writable:true,value:Date.prototype.valueOf})
Object.defineProperty(Date.prototype,"nv_getTime",{writable:true,value:Date.prototype.getTime})
Object.defineProperty(Date.prototype,"nv_getFullYear",{writable:true,value:Date.prototype.getFullYear})
Object.defineProperty(Date.prototype,"nv_getUTCFullYear",{writable:true,value:Date.prototype.getUTCFullYear})
Object.defineProperty(Date.prototype,"nv_getMonth",{writable:true,value:Date.prototype.getMonth})
Object.defineProperty(Date.prototype,"nv_getUTCMonth",{writable:true,value:Date.prototype.getUTCMonth})
Object.defineProperty(Date.prototype,"nv_getDate",{writable:true,value:Date.prototype.getDate})
Object.defineProperty(Date.prototype,"nv_getUTCDate",{writable:true,value:Date.prototype.getUTCDate})
Object.defineProperty(Date.prototype,"nv_getDay",{writable:true,value:Date.prototype.getDay})
Object.defineProperty(Date.prototype,"nv_getUTCDay",{writable:true,value:Date.prototype.getUTCDay})
Object.defineProperty(Date.prototype,"nv_getHours",{writable:true,value:Date.prototype.getHours})
Object.defineProperty(Date.prototype,"nv_getUTCHours",{writable:true,value:Date.prototype.getUTCHours})
Object.defineProperty(Date.prototype,"nv_getMinutes",{writable:true,value:Date.prototype.getMinutes})
Object.defineProperty(Date.prototype,"nv_getUTCMinutes",{writable:true,value:Date.prototype.getUTCMinutes})
Object.defineProperty(Date.prototype,"nv_getSeconds",{writable:true,value:Date.prototype.getSeconds})
Object.defineProperty(Date.prototype,"nv_getUTCSeconds",{writable:true,value:Date.prototype.getUTCSeconds})
Object.defineProperty(Date.prototype,"nv_getMilliseconds",{writable:true,value:Date.prototype.getMilliseconds})
Object.defineProperty(Date.prototype,"nv_getUTCMilliseconds",{writable:true,value:Date.prototype.getUTCMilliseconds})
Object.defineProperty(Date.prototype,"nv_getTimezoneOffset",{writable:true,value:Date.prototype.getTimezoneOffset})
Object.defineProperty(Date.prototype,"nv_setTime",{writable:true,value:Date.prototype.setTime})
Object.defineProperty(Date.prototype,"nv_setMilliseconds",{writable:true,value:Date.prototype.setMilliseconds})
Object.defineProperty(Date.prototype,"nv_setUTCMilliseconds",{writable:true,value:Date.prototype.setUTCMilliseconds})
Object.defineProperty(Date.prototype,"nv_setSeconds",{writable:true,value:Date.prototype.setSeconds})
Object.defineProperty(Date.prototype,"nv_setUTCSeconds",{writable:true,value:Date.prototype.setUTCSeconds})
Object.defineProperty(Date.prototype,"nv_setMinutes",{writable:true,value:Date.prototype.setMinutes})
Object.defineProperty(Date.prototype,"nv_setUTCMinutes",{writable:true,value:Date.prototype.setUTCMinutes})
Object.defineProperty(Date.prototype,"nv_setHours",{writable:true,value:Date.prototype.setHours})
Object.defineProperty(Date.prototype,"nv_setUTCHours",{writable:true,value:Date.prototype.setUTCHours})
Object.defineProperty(Date.prototype,"nv_setDate",{writable:true,value:Date.prototype.setDate})
Object.defineProperty(Date.prototype,"nv_setUTCDate",{writable:true,value:Date.prototype.setUTCDate})
Object.defineProperty(Date.prototype,"nv_setMonth",{writable:true,value:Date.prototype.setMonth})
Object.defineProperty(Date.prototype,"nv_setUTCMonth",{writable:true,value:Date.prototype.setUTCMonth})
Object.defineProperty(Date.prototype,"nv_setFullYear",{writable:true,value:Date.prototype.setFullYear})
Object.defineProperty(Date.prototype,"nv_setUTCFullYear",{writable:true,value:Date.prototype.setUTCFullYear})
Object.defineProperty(Date.prototype,"nv_toUTCString",{writable:true,value:Date.prototype.toUTCString})
Object.defineProperty(Date.prototype,"nv_toISOString",{writable:true,value:Date.prototype.toISOString})
Object.defineProperty(Date.prototype,"nv_toJSON",{writable:true,value:Date.prototype.toJSON})
}
var nf_init_RegExp=function(){
Object.defineProperty(RegExp.prototype,"nv_constructor",{writable:true,value:"RegExp"})
Object.defineProperty(RegExp.prototype,"nv_exec",{writable:true,value:RegExp.prototype.exec})
Object.defineProperty(RegExp.prototype,"nv_test",{writable:true,value:RegExp.prototype.test})
Object.defineProperty(RegExp.prototype,"nv_toString",{writable:true,value:RegExp.prototype.toString})
Object.defineProperty(RegExp.prototype,"nv_source",{get:function(){return this.source;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_global",{get:function(){return this.global;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_ignoreCase",{get:function(){return this.ignoreCase;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_multiline",{get:function(){return this.multiline;},set:function(){}});
Object.defineProperty(RegExp.prototype,"nv_lastIndex",{get:function(){return this.lastIndex;},set:function(v){this.lastIndex=v;}});
}
nf_init();
var nv_getDate=function(){var args=Array.prototype.slice.call(arguments);args.unshift(Date);return new(Function.prototype.bind.apply(Date, args));}
var nv_getRegExp=function(){var args=Array.prototype.slice.call(arguments);args.unshift(RegExp);return new(Function.prototype.bind.apply(RegExp, args));}
var nv_console={}
nv_console.nv_log=function(){var res="WXSRT:";for(var i=0;i<arguments.length;++i)res+=arguments[i]+" ";console.log(res);}
var nv_parseInt = parseInt, nv_parseFloat = parseFloat, nv_isNaN = isNaN, nv_isFinite = isFinite, nv_decodeURI = decodeURI, nv_decodeURIComponent = decodeURIComponent, nv_encodeURI = encodeURI, nv_encodeURIComponent = encodeURIComponent;
function $gdc(o,p,r) {
o=wh.rv(o);
if(o===null||o===undefined) return o;
if(o.constructor===String||o.constructor===Boolean||o.constructor===Number) return o;
if(o.constructor===Object){
var copy={};
for(var k in o)
if(o.hasOwnProperty(k))
if(undefined===p) copy[k.substring(3)]=$gdc(o[k],p,r);
else copy[p+k]=$gdc(o[k],p,r);
return copy;
}
if(o.constructor===Array){
var copy=[];
for(var i=0;i<o.length;i++) copy.push($gdc(o[i],p,r));
return copy;
}
if(o.constructor===Date){
var copy=new Date();
copy.setTime(o.getTime());
return copy;
}
if(o.constructor===RegExp){
var f="";
if(o.global) f+="g";
if(o.ignoreCase) f+="i";
if(o.multiline) f+="m";
return (new RegExp(o.source,f));
}
if(r&&o.constructor===Function){
if ( r == 1 ) return $gdc(o(),undefined, 2);
if ( r == 2 ) return o;
}
return null;
}
var nv_JSON={}
nv_JSON.nv_stringify=function(o){
JSON.stringify(o);
return JSON.stringify($gdc(o));
}
nv_JSON.nv_parse=function(o){
if(o===undefined) return undefined;
var t=JSON.parse(o);
return $gdc(t,'nv_');
}

function _af(p, a, c){
p.extraAttr = {"t_action": a, "t_cid": c};
}

function _gv( )
{if( typeof( window.__webview_engine_version__) == 'undefined' ) return 0.0;
return window.__webview_engine_version__;}
function _ai(i,p,e,me,r,c){var x=_grp(p,e,me);if(x)i.push(x);else{i.push('');_wp(me+':import:'+r+':'+c+': Path `'+p+'` not found from `'+me+'`.')}}
function _grp(p,e,me){if(p[0]!='/'){var mepart=me.split('/');mepart.pop();var ppart=p.split('/');for(var i=0;i<ppart.length;i++){if( ppart[i]=='..')mepart.pop();else if(!ppart[i]||ppart[i]=='.')continue;else mepart.push(ppart[i]);}p=mepart.join('/');}if(me[0]=='.'&&p[0]=='/')p='.'+p;if(e[p])return p;if(e[p+'.wxml'])return p+'.wxml';}
function _gd(p,c,e,d){if(!c)return;if(d[p][c])return d[p][c];for(var x=e[p].i.length-1;x>=0;x--){if(e[p].i[x]&&d[e[p].i[x]][c])return d[e[p].i[x]][c]};for(var x=e[p].ti.length-1;x>=0;x--){var q=_grp(e[p].ti[x],e,p);if(q&&d[q][c])return d[q][c]}var ii=_gapi(e,p);for(var x=0;x<ii.length;x++){if(ii[x]&&d[ii[x]][c])return d[ii[x]][c]}for(var k=e[p].j.length-1;k>=0;k--)if(e[p].j[k]){for(var q=e[e[p].j[k]].ti.length-1;q>=0;q--){var pp=_grp(e[e[p].j[k]].ti[q],e,p);if(pp&&d[pp][c]){return d[pp][c]}}}}
function _gapi(e,p){if(!p)return [];if($gaic[p]){return $gaic[p]};var ret=[],q=[],h=0,t=0,put={},visited={};q.push(p);visited[p]=true;t++;while(h<t){var a=q[h++];for(var i=0;i<e[a].ic.length;i++){var nd=e[a].ic[i];var np=_grp(nd,e,a);if(np&&!visited[np]){visited[np]=true;q.push(np);t++;}}for(var i=0;a!=p&&i<e[a].ti.length;i++){var ni=e[a].ti[i];var nm=_grp(ni,e,a);if(nm&&!put[nm]){put[nm]=true;ret.push(nm);}}}$gaic[p]=ret;return ret;}
var $ixc={};function _ic(p,ent,me,e,s,r,gg){var x=_grp(p,ent,me);ent[me].j.push(x);if(x){if($ixc[x]){_wp('-1:include:-1:-1: `'+p+'` is being included in a loop, will be stop.');return;}$ixc[x]=true;try{ent[x].f(e,s,r,gg)}catch(e){}$ixc[x]=false;}else{_wp(me+':include:-1:-1: Included path `'+p+'` not found from `'+me+'`.')}}
function _w(tn,f,line,c){_wp(f+':template:'+line+':'+c+': Template `'+tn+'` not found.');}function _ev(dom){var changed=false;delete dom.properities;delete dom.n;if(dom.children){do{changed=false;var newch = [];for(var i=0;i<dom.children.length;i++){var ch=dom.children[i];if( ch.tag=='virtual'){changed=true;for(var j=0;ch.children&&j<ch.children.length;j++){newch.push(ch.children[j]);}}else { newch.push(ch); } } dom.children = newch; }while(changed);for(var i=0;i<dom.children.length;i++){_ev(dom.children[i]);}} return dom; }
function _tsd( root )
{
if( root.tag == "wx-wx-scope" ) 
{
root.tag = "virtual";
root.wxCkey = "11";
root['wxScopeData'] = root.attr['wx:scope-data'];
delete root.n;
delete root.raw;
delete root.generics;
delete root.attr;
}
for( var i = 0 ; root.children && i < root.children.length ; i++ )
{
_tsd( root.children[i] );
}
return root;
}

var e_={}
if(typeof(global.entrys)==='undefined')global.entrys={};e_=global.entrys;
var d_={}
if(typeof(global.defines)==='undefined')global.defines={};d_=global.defines;
var f_={}
if(typeof(global.modules)==='undefined')global.modules={};f_=global.modules || {};
var p_={}
__WXML_GLOBAL__.ops_cached = __WXML_GLOBAL__.ops_cached || {}
__WXML_GLOBAL__.ops_set = __WXML_GLOBAL__.ops_set || {};
__WXML_GLOBAL__.ops_init = __WXML_GLOBAL__.ops_init || {};
var z=__WXML_GLOBAL__.ops_set.$gwx || [];
function gz$gwx_1(){
if( __WXML_GLOBAL__.ops_cached.$gwx_1)return __WXML_GLOBAL__.ops_cached.$gwx_1
__WXML_GLOBAL__.ops_cached.$gwx_1=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
})(__WXML_GLOBAL__.ops_cached.$gwx_1);return __WXML_GLOBAL__.ops_cached.$gwx_1
}
function gz$gwx_2(){
if( __WXML_GLOBAL__.ops_cached.$gwx_2)return __WXML_GLOBAL__.ops_cached.$gwx_2
__WXML_GLOBAL__.ops_cached.$gwx_2=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'78c8b366'])
Z([3,'_view M78c8b366'])
Z([3,'handleProxy'])
Z([3,'_view M78c8b366 content'])
Z([[7],[3,'$k']])
Z([1,'78c8b366-3'])
Z([a,[3,'_scroll-view M78c8b366 msg-list '],[[4],[[5],[[2,'?:'],[[7],[3,'tuchuflag']],[1,'tuchu'],[1,'']]]]])
Z([[7],[3,'scrollToView']])
Z([[7],[3,'scrollTop']])
Z([[7],[3,'scrollAnimation']])
Z([3,'true'])
Z([3,'50'])
Z([3,'index'])
Z([3,'row'])
Z([[7],[3,'msgList']])
Z(z[12])
Z([3,'_view M78c8b366 row'])
Z([[2,'+'],[1,'msg'],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'id']]])
Z([[7],[3,'index']])
Z([[2,'=='],[[6],[[7],[3,'row']],[3,'type']],[1,'system']])
Z([3,'_view M78c8b366 system'])
Z([[2,'=='],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'type']],[1,'text']])
Z([3,'_view M78c8b366 text'])
Z([a,[[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'content']],[3,'text']]])
Z([[2,'=='],[[6],[[7],[3,'row']],[3,'type']],[1,'user']])
Z([[2,'=='],[[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'userinfo']],[3,'uid']],[[7],[3,'myuid']]])
Z([3,'_view M78c8b366 my'])
Z([3,'_view M78c8b366 left'])
Z(z[21])
Z([3,'_view M78c8b366 bubble'])
Z([3,'_rich-text M78c8b366'])
Z([[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'content']],[3,'text']])
Z([3,'_view M78c8b366 rowss'])
Z([[2,'=='],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'type']],[1,'voice']])
Z(z[2])
Z([a,[3,'_view M78c8b366 bubble voice '],[[2,'?:'],[[2,'=='],[[7],[3,'playMsgid']],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'id']]],[1,'play'],[1,'']]])
Z(z[4])
Z([[2,'+'],[1,'78c8b366-0-'],[[7],[3,'index']]])
Z([3,'_view M78c8b366 length'])
Z([a,[[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'content']],[3,'length']]])
Z([3,'_view M78c8b366 icon my-voice'])
Z(z[32])
Z([[2,'!='],[[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'userinfo']],[3,'uid']],[[7],[3,'myuid']]])
Z([3,'_view M78c8b366 other'])
Z(z[27])
Z([3,'_image M78c8b366'])
Z([[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'userinfo']],[3,'face']])
Z([3,'_view M78c8b366 right'])
Z(z[21])
Z([a,[3,'_view M78c8b366 bubble position_bubble '],[[4],[[5],[[2,'?:'],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'flag_descript']],[1,'background_active'],[1,'']]]]])
Z([3,'_div M78c8b366 cotent_text'])
Z(z[30])
Z(z[31])
Z([3,'_div M78c8b366 content_text_position'])
Z([[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'flag_descript']])
Z([3,'_view M78c8b366 problem_list'])
Z([3,'_view M78c8b366 problem_list_item  title'])
Z([3,'_view M78c8b366 item_img'])
Z(z[45])
Z([3,'../../static/img/list/title.png'])
Z(z[1])
Z([a,[[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'descript']],[3,'title']]])
Z([3,'index0'])
Z([3,'item'])
Z([[6],[[6],[[6],[[7],[3,'row']],[3,'msg']],[3,'descript']],[3,'lists']])
Z(z[2])
Z([3,'_view M78c8b366 problem_list_item'])
Z(z[4])
Z([[2,'+'],[[2,'+'],[[2,'+'],[1,'78c8b366-1-'],[[7],[3,'index']]],[1,'-']],[[7],[3,'index0']]])
Z([3,'_view M78c8b366 problem_list_item_img'])
Z(z[45])
Z([3,'../../static/img/list/dian.png'])
Z(z[1])
Z([a,[[6],[[7],[3,'item']],[3,'label']]])
Z(z[33])
Z(z[2])
Z([a,z[35][1],z[35][2]])
Z(z[4])
Z([[2,'+'],[1,'78c8b366-2-'],[[7],[3,'index']]])
Z([3,'_view M78c8b366 icon other-voice'])
Z(z[38])
Z([a,z[39][1]])
Z(z[2])
Z([a,[3,'_view M78c8b366 input-box '],z[6][2]])
Z(z[4])
Z([1,'78c8b366-7'])
Z([3,'textarea'])
Z([3,'_view M78c8b366 textbox'])
Z(z[2])
Z(z[2])
Z(z[2])
Z(z[2])
Z([a,[3,'_view M78c8b366 voice-mode '],[[4],[[5],[[5],[[2,'?:'],[[7],[3,'isVoice']],[1,''],[1,'hidden']]],[[2,'?:'],[[7],[3,'recording']],[1,'recording'],[1,'']]]]])
Z(z[4])
Z([1,'78c8b366-4'])
Z([a,[[7],[3,'voiceTis']]])
Z([a,[3,'_view M78c8b366 text-mode '],[[2,'?:'],[[7],[3,'isVoice']],[1,'hidden'],[1,'']]])
Z([3,'_view M78c8b366 box'])
Z(z[10])
Z(z[2])
Z([3,'_textarea M78c8b366'])
Z(z[4])
Z([1,'78c8b366-5'])
Z([[7],[3,'textMsg']])
Z(z[2])
Z([a,[3,'_view M78c8b366 send '],z[96][2]])
Z(z[4])
Z([1,'78c8b366-6'])
Z([3,'_view M78c8b366 btn'])
Z([3,'发送'])
Z([a,[3,'_view M78c8b366 record '],[[2,'?:'],[[7],[3,'recording']],[1,''],[1,'hidden']]])
Z([a,[3,'_view M78c8b366 ing '],[[2,'?:'],[[7],[3,'willStop']],[1,'hidden'],[1,'']]])
Z([3,'_view M78c8b366 icon luyin2'])
Z([a,[3,'_view M78c8b366 cancel '],[[2,'?:'],[[7],[3,'willStop']],[1,''],[1,'hidden']]])
Z([3,'_view M78c8b366 icon chehui'])
Z([a,[3,'_view M78c8b366 tis '],[[2,'?:'],[[7],[3,'willStop']],[1,'change'],[1,'']]])
Z([a,[[7],[3,'recordTis']]])
})(__WXML_GLOBAL__.ops_cached.$gwx_2);return __WXML_GLOBAL__.ops_cached.$gwx_2
}
function gz$gwx_3(){
if( __WXML_GLOBAL__.ops_cached.$gwx_3)return __WXML_GLOBAL__.ops_cached.$gwx_3
__WXML_GLOBAL__.ops_cached.$gwx_3=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([[9],[[10],[[6],[[7],[3,'$root']],[1,'0']]],[[8],'$root',[[7],[3,'$root']]]])
Z([3,'78c8b366'])
})(__WXML_GLOBAL__.ops_cached.$gwx_3);return __WXML_GLOBAL__.ops_cached.$gwx_3
}
function gz$gwx_4(){
if( __WXML_GLOBAL__.ops_cached.$gwx_4)return __WXML_GLOBAL__.ops_cached.$gwx_4
__WXML_GLOBAL__.ops_cached.$gwx_4=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([3,'530cf7aa'])
Z([3,'_view M530cf7aa content'])
Z([3,'_view M530cf7aa content_header'])
Z([3,'_view M530cf7aa main'])
Z([3,'_view M530cf7aa header'])
Z([[7],[3,'autoplay']])
Z([3,'_swiper M530cf7aa swiper'])
Z([[7],[3,'duration']])
Z([[7],[3,'indicatorDots']])
Z([[7],[3,'interval']])
Z([3,'_swiper-item M530cf7aa'])
Z([3,'_view M530cf7aa swiper-item '])
Z([3,'_image M530cf7aa'])
Z([3,'aspectFill'])
Z([3,'../../static/img/home/logo1.png'])
Z(z[10])
Z(z[11])
Z(z[12])
Z(z[13])
Z(z[14])
Z([3,'_view M530cf7aa middle'])
Z([3,'已经有'])
Z([3,'_view M530cf7aa number'])
Z([3,'index'])
Z([3,'item'])
Z([[7],[3,'number_lists']])
Z(z[23])
Z([3,'_view M530cf7aa number_item'])
Z([[7],[3,'index']])
Z([a,[[7],[3,'item']]])
Z([3,'人获得专业解答'])
Z([3,'_view M530cf7aa bottom'])
Z([3,'_view M530cf7aa title'])
Z([3,'-- 热门咨询问题 --'])
Z([3,'_view M530cf7aa bottom_lists'])
Z([3,'100%'])
Z(z[23])
Z(z[24])
Z([[7],[3,'lists']])
Z(z[23])
Z([3,'handleProxy'])
Z([3,'_view M530cf7aa bottom_lists_item'])
Z([[7],[3,'$k']])
Z([[2,'+'],[1,'530cf7aa-0-'],[[7],[3,'index']]])
Z(z[28])
Z(z[12])
Z([3,'aspectFit'])
Z([[6],[[7],[3,'item']],[3,'img']])
Z(z[32])
Z([a,[[6],[[7],[3,'item']],[3,'title']]])
Z([3,'_view M530cf7aa about'])
Z([3,'_view M530cf7aa about_us'])
Z([3,'_view M530cf7aa left'])
Z(z[12])
Z(z[46])
Z([3,'../../static/img/home/about_us_32_f4ea2a.png'])
Z([3,'_view M530cf7aa right'])
Z([3,'_view M530cf7aa right_top'])
Z([3,'关于我们'])
Z([3,'_view M530cf7aa right_bottom'])
Z([3,'了解团队'])
Z([3,'_view M530cf7aa about_rabbit'])
Z(z[52])
Z(z[12])
Z(z[46])
Z([3,'../../static/img/home/jqr.png'])
Z(z[56])
Z(z[57])
Z([3,'了解机器人'])
Z(z[59])
Z([3,'机器人的成长记录'])
Z([3,'_view M530cf7aa about_logo'])
Z([3,'_view M530cf7aa logo'])
Z(z[12])
Z([3,'../../static/img/home/jqr_128.png'])
Z([3,'_view M530cf7aa about_right'])
Z([3,'_view M530cf7aa about_right_top'])
Z([3,'智能AI机器人'])
Z([3,'_view M530cf7aa about_right_bottom'])
Z([3,'只需3秒，快速获取专业咨询'])
Z([3,'_view M530cf7aa foot'])
Z([3,'技术支持：成潮科技有限公司'])
})(__WXML_GLOBAL__.ops_cached.$gwx_4);return __WXML_GLOBAL__.ops_cached.$gwx_4
}
function gz$gwx_5(){
if( __WXML_GLOBAL__.ops_cached.$gwx_5)return __WXML_GLOBAL__.ops_cached.$gwx_5
__WXML_GLOBAL__.ops_cached.$gwx_5=[];
(function(z){var a=11;function Z(ops){z.push(ops)}
Z([[9],[[10],[[6],[[7],[3,'$root']],[1,'0']]],[[8],'$root',[[7],[3,'$root']]]])
Z([3,'530cf7aa'])
})(__WXML_GLOBAL__.ops_cached.$gwx_5);return __WXML_GLOBAL__.ops_cached.$gwx_5
}
__WXML_GLOBAL__.ops_set.$gwx=z;
__WXML_GLOBAL__.ops_init.$gwx=true;
var nv_require=function(){var nnm={};var nom={};return function(n){return function(){if(!nnm[n]) return undefined;try{if(!nom[n])nom[n]=nnm[n]();return nom[n];}catch(e){e.message=e.message.replace(/nv_/g,'');var tmp = e.stack.substring(0,e.stack.lastIndexOf(n));e.stack = tmp.substring(0,tmp.lastIndexOf('\n'));e.stack = e.stack.replace(/\snv_/g,' ');e.stack = $gstack(e.stack);e.stack += '\n    at ' + n.substring(2);console.error(e);}
}}}()
var x=['./common/slots.wxml','./pages/HM-chat/HM-chat.vue.wxml','./pages/HM-chat/HM-chat.wxml','./HM-chat.vue.wxml','./pages/home/home.vue.wxml','./pages/home/home.wxml','./home.vue.wxml'];d_[x[0]]={}
var m0=function(e,s,r,gg){
var z=gz$gwx_1()
return r
}
e_[x[0]]={f:m0,j:[],i:[],ti:[],ic:[]}
d_[x[1]]={}
d_[x[1]]["78c8b366"]=function(e,s,r,gg){
var z=gz$gwx_2()
var b=x[1]+':78c8b366'
r.wxVkey=b
gg.f=$gdc(f_["./pages/HM-chat/HM-chat.vue.wxml"],"",1)
if(p_[b]){_wl(b,x[1]);return}
p_[b]=true
try{
var oB=_n('view')
_rz(z,oB,'class',1,e,s,gg)
var xC=_mz(z,'view',['bindtouchstart',2,'class',1,'data-comkey',2,'data-eventid',3],[],e,s,gg)
var oD=_mz(z,'scroll-view',['class',6,'scrollIntoView',1,'scrollTop',2,'scrollWithAnimation',3,'scrollY',4,'upperThreshold',5],[],e,s,gg)
var fE=_v()
_(oD,fE)
var cF=function(oH,hG,cI,gg){
var lK=_mz(z,'view',['class',16,'id',1,'key',2],[],oH,hG,gg)
var aL=_v()
_(lK,aL)
if(_oz(z,19,oH,hG,gg)){aL.wxVkey=1
var eN=_n('view')
_rz(z,eN,'class',20,oH,hG,gg)
var bO=_v()
_(eN,bO)
if(_oz(z,21,oH,hG,gg)){bO.wxVkey=1
var oP=_n('view')
_rz(z,oP,'class',22,oH,hG,gg)
var xQ=_oz(z,23,oH,hG,gg)
_(oP,xQ)
_(bO,oP)
}
bO.wxXCkey=1
_(aL,eN)
}
var tM=_v()
_(lK,tM)
if(_oz(z,24,oH,hG,gg)){tM.wxVkey=1
var oR=_v()
_(tM,oR)
if(_oz(z,25,oH,hG,gg)){oR.wxVkey=1
var cT=_n('view')
_rz(z,cT,'class',26,oH,hG,gg)
var hU=_n('view')
_rz(z,hU,'class',27,oH,hG,gg)
var oV=_v()
_(hU,oV)
if(_oz(z,28,oH,hG,gg)){oV.wxVkey=1
var oX=_n('view')
_rz(z,oX,'class',29,oH,hG,gg)
var lY=_mz(z,'rich-text',['class',30,'nodes',1],[],oH,hG,gg)
_(oX,lY)
var aZ=_n('view')
_rz(z,aZ,'class',32,oH,hG,gg)
_(oX,aZ)
_(oV,oX)
}
var cW=_v()
_(hU,cW)
if(_oz(z,33,oH,hG,gg)){cW.wxVkey=1
var t1=_mz(z,'view',['bindtap',34,'class',1,'data-comkey',2,'data-eventid',3],[],oH,hG,gg)
var e2=_n('view')
_rz(z,e2,'class',38,oH,hG,gg)
var b3=_oz(z,39,oH,hG,gg)
_(e2,b3)
_(t1,e2)
var o4=_n('view')
_rz(z,o4,'class',40,oH,hG,gg)
_(t1,o4)
var x5=_n('view')
_rz(z,x5,'class',41,oH,hG,gg)
_(t1,x5)
_(cW,t1)
}
oV.wxXCkey=1
cW.wxXCkey=1
_(cT,hU)
_(oR,cT)
}
var fS=_v()
_(tM,fS)
if(_oz(z,42,oH,hG,gg)){fS.wxVkey=1
var o6=_n('view')
_rz(z,o6,'class',43,oH,hG,gg)
var f7=_n('view')
_rz(z,f7,'class',44,oH,hG,gg)
var c8=_mz(z,'image',['class',45,'src',1],[],oH,hG,gg)
_(f7,c8)
_(o6,f7)
var h9=_n('view')
_rz(z,h9,'class',47,oH,hG,gg)
var o0=_v()
_(h9,o0)
if(_oz(z,48,oH,hG,gg)){o0.wxVkey=1
var oBB=_n('view')
_rz(z,oBB,'class',49,oH,hG,gg)
var lCB=_n('view')
_rz(z,lCB,'class',50,oH,hG,gg)
var tEB=_mz(z,'rich-text',['class',51,'nodes',1],[],oH,hG,gg)
_(lCB,tEB)
var eFB=_n('view')
_rz(z,eFB,'class',53,oH,hG,gg)
_(lCB,eFB)
var aDB=_v()
_(lCB,aDB)
if(_oz(z,54,oH,hG,gg)){aDB.wxVkey=1
var bGB=_n('view')
_rz(z,bGB,'class',55,oH,hG,gg)
var oHB=_n('view')
_rz(z,oHB,'class',56,oH,hG,gg)
var xIB=_n('view')
_rz(z,xIB,'class',57,oH,hG,gg)
var oJB=_mz(z,'image',['class',58,'src',1],[],oH,hG,gg)
_(xIB,oJB)
_(oHB,xIB)
var fKB=_n('view')
_rz(z,fKB,'class',60,oH,hG,gg)
var cLB=_oz(z,61,oH,hG,gg)
_(fKB,cLB)
_(oHB,fKB)
_(bGB,oHB)
var hMB=_v()
_(bGB,hMB)
var oNB=function(oPB,cOB,lQB,gg){
var tSB=_mz(z,'view',['bindtap',65,'class',1,'data-comkey',2,'data-eventid',3],[],oPB,cOB,gg)
var eTB=_n('view')
_rz(z,eTB,'class',69,oPB,cOB,gg)
var bUB=_mz(z,'image',['class',70,'src',1],[],oPB,cOB,gg)
_(eTB,bUB)
_(tSB,eTB)
var oVB=_n('view')
_rz(z,oVB,'class',72,oPB,cOB,gg)
var xWB=_oz(z,73,oPB,cOB,gg)
_(oVB,xWB)
_(tSB,oVB)
_(lQB,tSB)
return lQB
}
hMB.wxXCkey=2
_2z(z,64,oNB,oH,hG,gg,hMB,'item','index0','')
_(aDB,bGB)
}
aDB.wxXCkey=1
_(oBB,lCB)
_(o0,oBB)
}
var cAB=_v()
_(h9,cAB)
if(_oz(z,74,oH,hG,gg)){cAB.wxVkey=1
var oXB=_mz(z,'view',['bindtap',75,'class',1,'data-comkey',2,'data-eventid',3],[],oH,hG,gg)
var fYB=_n('view')
_rz(z,fYB,'class',79,oH,hG,gg)
_(oXB,fYB)
var cZB=_n('view')
_rz(z,cZB,'class',80,oH,hG,gg)
var h1B=_oz(z,81,oH,hG,gg)
_(cZB,h1B)
_(oXB,cZB)
_(cAB,oXB)
}
o0.wxXCkey=1
cAB.wxXCkey=1
_(o6,h9)
_(fS,o6)
}
oR.wxXCkey=1
fS.wxXCkey=1
}
aL.wxXCkey=1
tM.wxXCkey=1
_(cI,lK)
return cI
}
fE.wxXCkey=2
_2z(z,14,cF,e,s,gg,fE,'row','index','index')
_(xC,oD)
_(oB,xC)
var o2B=_mz(z,'view',['catchtouchmove',82,'class',1,'data-comkey',2,'data-eventid',3,'id',4],[],e,s,gg)
var c3B=_n('view')
_rz(z,c3B,'class',87,e,s,gg)
var o4B=_mz(z,'view',['bindtouchcancel',88,'bindtouchend',1,'bindtouchstart',2,'catchtouchmove',3,'class',4,'data-comkey',5,'data-eventid',6],[],e,s,gg)
var l5B=_oz(z,95,e,s,gg)
_(o4B,l5B)
_(c3B,o4B)
var a6B=_n('view')
_rz(z,a6B,'class',96,e,s,gg)
var t7B=_n('view')
_rz(z,t7B,'class',97,e,s,gg)
var e8B=_mz(z,'textarea',['autoHeight',98,'bindinput',1,'class',2,'data-comkey',3,'data-eventid',4,'value',5],[],e,s,gg)
_(t7B,e8B)
_(a6B,t7B)
_(c3B,a6B)
_(o2B,c3B)
var b9B=_mz(z,'view',['bindtap',104,'class',1,'data-comkey',2,'data-eventid',3],[],e,s,gg)
var o0B=_n('view')
_rz(z,o0B,'class',108,e,s,gg)
var xAC=_oz(z,109,e,s,gg)
_(o0B,xAC)
_(b9B,o0B)
_(o2B,b9B)
_(oB,o2B)
var oBC=_n('view')
_rz(z,oBC,'class',110,e,s,gg)
var fCC=_n('view')
_rz(z,fCC,'class',111,e,s,gg)
var cDC=_n('view')
_rz(z,cDC,'class',112,e,s,gg)
_(fCC,cDC)
_(oBC,fCC)
var hEC=_n('view')
_rz(z,hEC,'class',113,e,s,gg)
var oFC=_n('view')
_rz(z,oFC,'class',114,e,s,gg)
_(hEC,oFC)
_(oBC,hEC)
var cGC=_n('view')
_rz(z,cGC,'class',115,e,s,gg)
var oHC=_oz(z,116,e,s,gg)
_(cGC,oHC)
_(oBC,cGC)
_(oB,oBC)
_(r,oB)
}catch(err){
p_[b]=false
throw err
}
p_[b]=false
return r
}
var m1=function(e,s,r,gg){
var z=gz$gwx_2()
return r
}
e_[x[1]]={f:m1,j:[],i:[],ti:[],ic:[]}
d_[x[2]]={}
var m2=function(e,s,r,gg){
var z=gz$gwx_3()
var oD=e_[x[2]].i
_ai(oD,x[3],e_,x[2],1,1)
var fE=_v()
_(r,fE)
var cF=_oz(z,1,e,s,gg)
var hG=_gd(x[2],cF,e_,d_)
if(hG){
var oH=_1z(z,0,e,s,gg) || {}
var cur_globalf=gg.f
fE.wxXCkey=3
hG(oH,oH,fE,gg)
gg.f=cur_globalf
}
else _w(cF,x[2],2,18)
oD.pop()
return r
}
e_[x[2]]={f:m2,j:[],i:[],ti:[x[3]],ic:[]}
d_[x[4]]={}
d_[x[4]]["530cf7aa"]=function(e,s,r,gg){
var z=gz$gwx_4()
var b=x[4]+':530cf7aa'
r.wxVkey=b
gg.f=$gdc(f_["./pages/home/home.vue.wxml"],"",1)
if(p_[b]){_wl(b,x[4]);return}
p_[b]=true
try{
var oB=_n('view')
_rz(z,oB,'class',1,e,s,gg)
var xC=_n('view')
_rz(z,xC,'class',2,e,s,gg)
var oD=_n('view')
_rz(z,oD,'class',3,e,s,gg)
var fE=_n('view')
_rz(z,fE,'class',4,e,s,gg)
var cF=_mz(z,'swiper',['autoplay',5,'class',1,'duration',2,'indicatorDots',3,'interval',4],[],e,s,gg)
var hG=_n('swiper-item')
_rz(z,hG,'class',10,e,s,gg)
var oH=_n('view')
_rz(z,oH,'class',11,e,s,gg)
var cI=_mz(z,'image',['class',12,'mode',1,'src',2],[],e,s,gg)
_(oH,cI)
_(hG,oH)
_(cF,hG)
var oJ=_n('swiper-item')
_rz(z,oJ,'class',15,e,s,gg)
var lK=_n('view')
_rz(z,lK,'class',16,e,s,gg)
var aL=_mz(z,'image',['class',17,'mode',1,'src',2],[],e,s,gg)
_(lK,aL)
_(oJ,lK)
_(cF,oJ)
_(fE,cF)
_(oD,fE)
var tM=_n('view')
_rz(z,tM,'class',20,e,s,gg)
var eN=_oz(z,21,e,s,gg)
_(tM,eN)
var bO=_n('view')
_rz(z,bO,'class',22,e,s,gg)
var oP=_v()
_(bO,oP)
var xQ=function(fS,oR,cT,gg){
var oV=_mz(z,'view',['class',27,'key',1],[],fS,oR,gg)
var cW=_oz(z,29,fS,oR,gg)
_(oV,cW)
_(cT,oV)
return cT
}
oP.wxXCkey=2
_2z(z,25,xQ,e,s,gg,oP,'item','index','index')
_(tM,bO)
var oX=_oz(z,30,e,s,gg)
_(tM,oX)
_(oD,tM)
var lY=_n('view')
_rz(z,lY,'class',31,e,s,gg)
var aZ=_n('view')
_rz(z,aZ,'class',32,e,s,gg)
var t1=_oz(z,33,e,s,gg)
_(aZ,t1)
_(lY,aZ)
var e2=_mz(z,'view',['class',34,'width',1],[],e,s,gg)
var b3=_v()
_(e2,b3)
var o4=function(o6,x5,f7,gg){
var h9=_mz(z,'view',['bindtap',40,'class',1,'data-comkey',2,'data-eventid',3,'key',4],[],o6,x5,gg)
var o0=_mz(z,'image',['class',45,'mode',1,'src',2],[],o6,x5,gg)
_(h9,o0)
var cAB=_n('view')
_rz(z,cAB,'class',48,o6,x5,gg)
var oBB=_oz(z,49,o6,x5,gg)
_(cAB,oBB)
_(h9,cAB)
_(f7,h9)
return f7
}
b3.wxXCkey=2
_2z(z,38,o4,e,s,gg,b3,'item','index','index')
_(lY,e2)
var lCB=_n('view')
_rz(z,lCB,'class',50,e,s,gg)
var aDB=_n('view')
_rz(z,aDB,'class',51,e,s,gg)
var tEB=_n('view')
_rz(z,tEB,'class',52,e,s,gg)
var eFB=_mz(z,'image',['class',53,'mode',1,'src',2],[],e,s,gg)
_(tEB,eFB)
_(aDB,tEB)
var bGB=_n('view')
_rz(z,bGB,'class',56,e,s,gg)
var oHB=_n('view')
_rz(z,oHB,'class',57,e,s,gg)
var xIB=_oz(z,58,e,s,gg)
_(oHB,xIB)
_(bGB,oHB)
var oJB=_n('view')
_rz(z,oJB,'class',59,e,s,gg)
var fKB=_oz(z,60,e,s,gg)
_(oJB,fKB)
_(bGB,oJB)
_(aDB,bGB)
_(lCB,aDB)
var cLB=_n('view')
_rz(z,cLB,'class',61,e,s,gg)
var hMB=_n('view')
_rz(z,hMB,'class',62,e,s,gg)
var oNB=_mz(z,'image',['class',63,'mode',1,'src',2],[],e,s,gg)
_(hMB,oNB)
_(cLB,hMB)
var cOB=_n('view')
_rz(z,cOB,'class',66,e,s,gg)
var oPB=_n('view')
_rz(z,oPB,'class',67,e,s,gg)
var lQB=_oz(z,68,e,s,gg)
_(oPB,lQB)
_(cOB,oPB)
var aRB=_n('view')
_rz(z,aRB,'class',69,e,s,gg)
var tSB=_oz(z,70,e,s,gg)
_(aRB,tSB)
_(cOB,aRB)
_(cLB,cOB)
_(lCB,cLB)
_(lY,lCB)
var eTB=_n('view')
_rz(z,eTB,'class',71,e,s,gg)
var bUB=_n('view')
_rz(z,bUB,'class',72,e,s,gg)
var oVB=_mz(z,'image',['class',73,'src',1],[],e,s,gg)
_(bUB,oVB)
_(eTB,bUB)
var xWB=_n('view')
_rz(z,xWB,'class',75,e,s,gg)
var oXB=_n('view')
_rz(z,oXB,'class',76,e,s,gg)
var fYB=_oz(z,77,e,s,gg)
_(oXB,fYB)
_(xWB,oXB)
var cZB=_n('view')
_rz(z,cZB,'class',78,e,s,gg)
var h1B=_oz(z,79,e,s,gg)
_(cZB,h1B)
_(xWB,cZB)
_(eTB,xWB)
_(lY,eTB)
_(oD,lY)
_(xC,oD)
_(oB,xC)
var o2B=_n('view')
_rz(z,o2B,'class',80,e,s,gg)
var c3B=_oz(z,81,e,s,gg)
_(o2B,c3B)
_(oB,o2B)
_(r,oB)
}catch(err){
p_[b]=false
throw err
}
p_[b]=false
return r
}
var m3=function(e,s,r,gg){
var z=gz$gwx_4()
return r
}
e_[x[4]]={f:m3,j:[],i:[],ti:[],ic:[]}
d_[x[5]]={}
var m4=function(e,s,r,gg){
var z=gz$gwx_5()
var lK=e_[x[5]].i
_ai(lK,x[6],e_,x[5],1,1)
var aL=_v()
_(r,aL)
var tM=_oz(z,1,e,s,gg)
var eN=_gd(x[5],tM,e_,d_)
if(eN){
var bO=_1z(z,0,e,s,gg) || {}
var cur_globalf=gg.f
aL.wxXCkey=3
eN(bO,bO,aL,gg)
gg.f=cur_globalf
}
else _w(tM,x[5],2,18)
lK.pop()
return r
}
e_[x[5]]={f:m4,j:[],i:[],ti:[x[6]],ic:[]}
if(path&&e_[path]){
window.__wxml_comp_version__=0.02
return function(env,dd,global){$gwxc=0;var root={"tag":"wx-page"};root.children=[]
var main=e_[path].f
if (typeof global==="undefined")global={};global.f=$gdc(f_[path],"",1);
if(typeof(window.__webview_engine_version__)!='undefined'&&window.__webview_engine_version__+1e-6>=0.02+1e-6&&window.__mergeData__)
{
env=window.__mergeData__(env,dd);
}
try{
main(env,{},root,global);
_tsd(root)
if(typeof(window.__webview_engine_version__)=='undefined'|| window.__webview_engine_version__+1e-6<0.01+1e-6){return _ev(root);}
}catch(err){
console.log(err)
}
return root;
}
}
}


var BASE_DEVICE_WIDTH = 750;
var isIOS=navigator.userAgent.match("iPhone");
var deviceWidth = window.screen.width || 375;
var deviceDPR = window.devicePixelRatio || 2;
var checkDeviceWidth = window.__checkDeviceWidth__ || function() {
var newDeviceWidth = window.screen.width || 375
var newDeviceDPR = window.devicePixelRatio || 2
var newDeviceHeight = window.screen.height || 375
if (window.screen.orientation && /^landscape/.test(window.screen.orientation.type || '')) newDeviceWidth = newDeviceHeight
if (newDeviceWidth !== deviceWidth || newDeviceDPR !== deviceDPR) {
deviceWidth = newDeviceWidth
deviceDPR = newDeviceDPR
}
}
checkDeviceWidth()
var eps = 1e-4;
var transformRPX = window.__transformRpx__ || function(number, newDeviceWidth) {
if ( number === 0 ) return 0;
number = number / BASE_DEVICE_WIDTH * ( newDeviceWidth || deviceWidth );
number = Math.floor(number + eps);
if (number === 0) {
if (deviceDPR === 1 || !isIOS) {
return 1;
} else {
return 0.5;
}
}
return number;
}
var setCssToHead = function(file, _xcInvalid, info) {
var Ca = {};
var css_id;
var info = info || {};
var _C= [[[2,1],],["@font-face { font-family: uniicons; font-weight: normal; font-style: normal; src: url(\x27https://img-cdn-qiniu.dcloud.net.cn/fonts/uni.ttf\x27) format(\x27truetype\x27); }\n",],];
function makeup(file, opt) {
var _n = typeof(file) === "number";
if ( _n && Ca.hasOwnProperty(file)) return "";
if ( _n ) Ca[file] = 1;
var ex = _n ? _C[file] : file;
var res="";
for (var i = ex.length - 1; i >= 0; i--) {
var content = ex[i];
if (typeof(content) === "object")
{
var op = content[0];
if ( op == 0 )
res = transformRPX(content[1], opt.deviceWidth) + "px" + res;
else if ( op == 1)
res = opt.suffix + res;
else if ( op == 2 ) 
res = makeup(content[1], opt) + res;
}
else
res = content + res
}
return res;
}
var rewritor = function(suffix, opt, style){
opt = opt || {};
suffix = suffix || "";
opt.suffix = suffix;
if ( opt.allowIllegalSelector != undefined && _xcInvalid != undefined )
{
if ( opt.allowIllegalSelector )
console.warn( "For developer:" + _xcInvalid );
else
{
console.error( _xcInvalid + "This wxss file is ignored." );
return;
}
}
Ca={};
css = makeup(file, opt);
if ( !style ) 
{
var head = document.head || document.getElementsByTagName('head')[0];
window.__rpxRecalculatingFuncs__ = window.__rpxRecalculatingFuncs__ || [];
style = document.createElement('style');
style.type = 'text/css';
style.setAttribute( "wxss:path", info.path );
head.appendChild(style);
window.__rpxRecalculatingFuncs__.push(function(size){
opt.deviceWidth = size.width;
rewritor(suffix, opt, style);
});
}
if (style.styleSheet) {
style.styleSheet.cssText = css;
} else {
if ( style.childNodes.length == 0 )
style.appendChild(document.createTextNode(css));
else 
style.childNodes[0].nodeValue = css;
}
}
return rewritor;
}
setCssToHead([])();setCssToHead([[2,0]],undefined,{path:"./app.wxss"})();

__wxAppCode__['app.wxss']=setCssToHead([[2,0]],undefined,{path:"./app.wxss"});    
__wxAppCode__['app.wxml']=$gwx('./app.wxml');

__wxAppCode__['pages/HM-chat/HM-chat.wxss']=setCssToHead(["@charset \x22UTF-8\x22;\nbody { background-color: #e5e5e5; }\n@font-face { font-family: \x22HMfont-home\x22; src: url(\x22data:application/x-font-woff2;charset\x3dutf-8;base64,d09GMgABAAAAAAn8AAsAAAAAE1wAAAmvAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHEIGVgCFDAqWYJI9ATYCJANACyIABCAFhFUHgV8bThBRlFFWNdkXBXbDsSFQq221McNWrxUbYqGhiTju98MTeXqNh/9fo90388cEMe0bwSOJRIjavZIgESqnE5J5JqQVDZH/XdNfoHSAjqKqRsA+Tf/Ruya33E/bkdHsJtycY2XWAGbM5oenzf173A3lHrEilsmMbu74Y5VmYtxpgza9DMxkWL0gfjGbGRE54AL2f2ut3h2u8Q7RaZRCjDPLIv8cfAUR30MtEUWbSReVJkk0RB4lWWkNg7WVA1sBKmIUdr0uzibQOmxA4vrWwQXkJUweKHPfdwXkA+FSik2o1aVizyTegEKdvWINwGv59bEGY9GeTJFjW95pswIrzz3LYi//0O4JEaDrY3DZjxwXgUR8V3IfIeXARaloVRXT3mK/tsi3LubcJfese8l96Xbd1l1ve2z7eJp5lv3zB7URSdJNYd3Dfm7UUxxkGu0sLFcbVEa5pP3D6/QmokQw3OGzfJp/2kBkLJYQDYuziJbFJUSweIkoWXQRNYuEGFi0BLzFOhAjS4+InKUPRGI5I2a+kg7VSWUGoXoos2BNmGIWexwFroD8IUD6C1A9lYp8F3ClwsFgcgPdNpN08v1czkEOJ4aeieaC3QyVfb9PX2kbn9/0CwTeNAm79H1Kc2x3i9C7LcEZtMSLfE6T4aM+YWOm06dZ5cm9I+xoYw+rqGlScKKlHytu9h6Dw0E5nXK7nbTZknT1jFldR9cuzNMz9Srf7FydqpYW5mRr6Dq1OC9HqzYzoiw1cjohV2tX1Ji1K9bSdVkEbaxS1xQwpmpVpUFheyyzPyGdbXKHexkByib+vtgeK1X75xKqWl+grUNIbRZDXT31tBMiPZAyF0KmniCQhSgACkh5+gIxtvTS/si+VjbAuY6SMdCzbqInzwkjk5ENzMCkNv+ghQQ0qSSAUGmAMQoBozoAIrUe6qpzM+tma1T1jDgvVzdBWcIcLT170njGQU3cCpnUTSdkHH3ltwPHpKotTIP6HH12Lvd4czCWgbJYhY1U5ddlTCICSs1is0in8tXExk7VVRuMQhIQGgOtFcolPmMkIqDVduTGEOn1jI4gFERmSUsv3rGmoKUEQLITLUyzqpFukq8T6U+omVQsT8XHxsnipPEyBAlKNmkNMlMJgOT5Tpsoo2RGP3lOTQyk5GRBgJKw2WQsarWzSa1aLF/+UBk2PkA3wEkBM/RwOLJ0ORWiVCR3YYAAFyIlAdaNqEnmh0sTqOsAq97R85Jt+HGHrNKWgDHmxOPxumKmRGzudayPtogu9D2Zx688C3D6XJSgpgF6MJbomdtyOYBgcXOGSgMAPXqy+F11pMYHlFLCkkKM0S1T+U5SN0Ynh39SxcxmTPNHrTFIuieyxYgZXSDUAPpLLT2ZciVvihOh05k+JIAjoL7HtNsVFc5Rl+1hgAAIlNqGX3GEK0llMm0nZUdmhQzymg3Q9j6yO4FQsmqtQbXmZ+z+sOynUrt3nmbeXu3MYW9f8y38128LpWAVeyLMz4cTORbEDPYKHU19Oyx0OF12GIhfEx+/RRIm2RzPeIPE2yYRM7HBWBx+GvANWXAlMYcmWriz1/Tt2bk+jq7CdOzMu5zsn3zZXwg2Gu14YCBuh3NggN0DI8BbJpCXZb2I4xh+kdAmbU0IA6HYquya81nqYSk87Xgi35ur4HnxZWEvnoLrzbOEjHmJiY2JjV6I8c4ynSEsJTKcHxuWYPRFFleV2Sbi0Dsk4XmDSToXTMnUnW/PW9J9W4UCgP+h0rTi9tiJd6qQgk2lPI/KKeybAPx+c7vZHdimbruzyCP9iZvd0VuBuIniuXirHQ8oG2IThFIUI8QOhjfNMg86GH4Bv4ixLlr4BDi2wDDwXTYYTgfnBJur1nAw2yGngw96JhQo+48cMWVE8kWwcA55ZuzwkSP/mpp9D6wFm2e1Bc8cPVraL2Ng7y6KfSNHqQfTYByYMmbT73WNmwZs6m8sBR54XCndTHwvu6v+8N+Jze9/jeGd8bpoHePtMv0/9U6e78bTtf+aly55P40cNtJ3PH3U6xQ9DkRNos+Chp2TpNwX4lZOwkTa4nOLPxpMLc8Sm0srSwD6Y1KW7ftPZ68x3DWS8d4cJbAKE6QJEfRrhAafMLV0RoCRLhKdBaJzNtzPD7dxLIgZ7Al4006exyHEYXMewjqApFokPRIu9FvLiPf96uWlpuZmRZKiH1i0OCNj1ar7zSDqYiRbCQsMrKUXZswxBkQEbCmv2RJgKK82+UcGbpk+0woVSxekQrYCzp4Hk30E3oHhAh+4fLcOPCfzOVu3cvKkHAWzNAVyjAyOQsrJix47n0OZpbTUDKdJp8CZs+BkAKfMnDkF+kJmmrcN4OSZs8CRuwZ+N76gampCxtj83XWO5X1GYc7hIypq+N32eTe6Wr/GfXW5GukBLnvJ1gEPhlmsuUHzg3Osp/vJCZ4flGsFf27fjV18spjdTfQUuVANcgldRA3hKhSUutCGgGhDaMo0tXMHwiUq3gG5entO2xmnECa3H53AjRpKFFYIK7qrHjMJ75sEC91BPlGc0TlZY9qlsdcuZaXy0D3hfz4cmLd2WzbK3Xhhdw7c2VLCxtxsFCMEo8bArEww9ruOrc5joK9g1xp85MghQ4wyuPV71+/tMVxAMmzA1lSt+WmbjFkwL/lV6az7APzZ5qvVmmy7b1bJGrTDhmRfMBYbWMZmNOu3bJdPlLL/5WOR2XZCTJpmU4mx8lv9Fg76T8NagO4vUacJ+n/Sr0b/LYb8+1z5QCb935a0m6WWYXzwh4DO2Sa9g2jEnJ6tYwTU5jp7N2RmaHkn/gjEb/fXpmpXbkpAGaAv7pnKAfdc6bg4GZx1L3QuQ8lVC3BvXbC8f2eHQEqkBuc9aO6h9849M3oPucrgAyQY/HEv7PYJJQy23Ft3/R+xczqmsHWDgrDCyzfcl1o5ehKxnUOr5Bm6NhTGR4u1rtDEvlZ8dGgklLeNCk3ZbeKaO0bkcMfoKt+6ng/DUPPI6AAlDXlE0dzwsKPadkjqKjDXGEgg4b2CK7vx65M0xSlPmNsOA58/g1xWSDDKeq/KV5AR89+zc6OGjKSKtxUqR4NtF47VuMZemcTBDQxGqzqqrXIMCnm2xkXq1QJIIkO8EpmROcOkIyevYmhUqurWBmgCe4U5WJFHiiLKqKKOJtrooo8hxphihl6g5bGv3MAXkfBvPaFbVq6ga4Uq+wWdEfo6NVTmr1oVkYoye2NvfCWLmYQx0sjozFSxszhZ4Ctjb7QtavLQDNa0L5HRZQYJYxrNLbJR4QhZvOV46Fm/lqB428nsrJSx/OwbEgYA\x22) format(\x22woff2\x22); }\n.",[1],"icon { font-family: \x22HMfont-home\x22 !important; font-size: ",[0,56],"; font-style: normal; color: #333; }\n.",[1],"icon.",[1],"biaoqing:before { content: \x22\\E797\x22; }\n.",[1],"icon.",[1],"jianpan:before { content: \x22\\E7B2\x22; }\n.",[1],"icon.",[1],"yuyin:before { content: \x22\\E805\x22; }\n.",[1],"icon.",[1],"tupian:before { content: \x22\\E639\x22; }\n.",[1],"icon.",[1],"chehui:before { content: \x22\\E904\x22; }\n.",[1],"icon.",[1],"luyin:before { content: \x22\\E905\x22; }\n.",[1],"icon.",[1],"luyin2:before { content: \x22\\E677\x22; }\n.",[1],"icon.",[1],"other-voice:before { content: \x22\\E667\x22; }\n.",[1],"icon.",[1],"my-voice:before { content: \x22\\E906\x22; }\n.",[1],"icon.",[1],"hongbao:before { content: \x22\\E626\x22; }\n.",[1],"icon.",[1],"tupian2:before { content: \x22\\E674\x22; }\n.",[1],"icon.",[1],"paizhao:before { content: \x22\\E63E\x22; }\n.",[1],"icon.",[1],"add:before { content: \x22\\E655\x22; }\n.",[1],"icon.",[1],"close:before { content: \x22\\E607\x22; }\n.",[1],"icon.",[1],"to:before { content: \x22\\E675\x22; }\n.",[1],"hidden { display: none !important; }\n.",[1],"popup-layer { -webkit-transition: all .15s linear; -o-transition: all .15s linear; transition: all .15s linear; width: 96%; height: 42vw; padding: ",[0,20]," 2%; background-color: #f3f3f3; border-top: solid ",[0,1]," #ddd; position: fixed; z-index: 20; top: 100%; }\n.",[1],"popup-layer.",[1],"showLayer { -webkit-transform: translate3d(0, -42vw, 0); transform: translate3d(0, -42vw, 0); }\n.",[1],"popup-layer .",[1],"emoji-swiper { height: 40vw; }\n.",[1],"popup-layer .",[1],"emoji-swiper wx-swiper-item { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-align-content: flex-start; -ms-flex-line-pack: start; align-content: flex-start; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; }\n.",[1],"popup-layer .",[1],"emoji-swiper wx-swiper-item wx-view { width: 12vw; height: 12vw; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"popup-layer .",[1],"emoji-swiper wx-swiper-item wx-view wx-image { width: 8.4vw; height: 8.4vw; }\n.",[1],"popup-layer .",[1],"more-layer { width: 100%; height: 42vw; }\n.",[1],"popup-layer .",[1],"more-layer .",[1],"list { width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; }\n.",[1],"popup-layer .",[1],"more-layer .",[1],"list .",[1],"box { width: 18vw; height: 18vw; border-radius: ",[0,20],"; background-color: #fff; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; margin: 0 3vw 2vw 3vw; }\n.",[1],"popup-layer .",[1],"more-layer .",[1],"list .",[1],"box .",[1],"icon { font-size: ",[0,70],"; }\n.",[1],"input-box { width: 98%; min-height: ",[0,100],"; padding: 0 1%; background-color: #f2f2f2; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; position: fixed; z-index: 20; bottom: ",[0,-2],"; -webkit-transition: all .15s linear; -o-transition: all .15s linear; transition: all .15s linear; border-bottom: solid ",[0,1]," #ddd; }\n.",[1],"input-box.",[1],"showLayer { -webkit-transform: translate3d(0, -42vw, 0); transform: translate3d(0, -42vw, 0); }\n.",[1],"input-box .",[1],"voice, .",[1],"input-box .",[1],"more { -webkit-flex-shrink: 0; -ms-flex-negative: 0; flex-shrink: 0; width: ",[0,90],"; height: ",[0,100],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"input-box .",[1],"send { z-index: 999; margin-left: ",[0,20],"; -webkit-flex-shrink: 0; -ms-flex-negative: 0; flex-shrink: 0; width: ",[0,100],"; height: ",[0,100],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"input-box .",[1],"send .",[1],"btn { z-index: 999; width: ",[0,90],"; height: ",[0,56],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; background: -webkit-gradient(linear, left top, right top, from(#63a9ef), to(#499ef4)); background: -o-linear-gradient(left, #63a9ef, #499ef4); background: linear-gradient(to right, #63a9ef, #499ef4); color: #fff; border-radius: ",[0,6],"; font-size: ",[0,24],"; }\n.",[1],"input-box .",[1],"textbox { width: 100%; min-height: ",[0,70],"; margin-top: ",[0,15],"; }\n.",[1],"input-box .",[1],"textbox .",[1],"voice-mode { width: calc(100% - ",[0,2],"); height: ",[0,68],"; border-radius: ",[0,70],"; border: solid ",[0,1]," #cdcdcd; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; font-size: ",[0,28],"; background-color: #fff; color: #555; }\n.",[1],"input-box .",[1],"textbox .",[1],"voice-mode.",[1],"recording { background-color: #e5e5e5; }\n.",[1],"input-box .",[1],"textbox .",[1],"text-mode { width: 100%; min-height: ",[0,70],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; background-color: #fff; border-radius: ",[0,40],"; }\n.",[1],"input-box .",[1],"textbox .",[1],"text-mode .",[1],"box { width: 100%; padding-left: ",[0,30],"; min-height: ",[0,70],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"input-box .",[1],"textbox .",[1],"text-mode .",[1],"box wx-textarea { width: 100%; }\n.",[1],"input-box .",[1],"textbox .",[1],"text-mode .",[1],"em { -webkit-flex-shrink: 0; -ms-flex-negative: 0; flex-shrink: 0; width: ",[0,80],"; padding-left: ",[0,10],"; height: ",[0,70],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"record { width: 40vw; height: 40vw; position: fixed; top: 55%; left: 30%; background-color: rgba(0, 0, 0, 0.6); border-radius: ",[0,20],"; }\n.",[1],"record .",[1],"ing { width: 100%; height: 30vw; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n@-webkit-keyframes volatility { 0% { background-position: 0% 130%; }\n20% { background-position: 0% 150%; }\n30% { background-position: 0% 155%; }\n40% { background-position: 0% 150%; }\n50% { background-position: 0% 145%; }\n70% { background-position: 0% 150%; }\n80% { background-position: 0% 155%; }\n90% { background-position: 0% 140%; }\n100% { background-position: 0% 135%; }\n}@keyframes volatility { 0% { background-position: 0% 130%; }\n20% { background-position: 0% 150%; }\n30% { background-position: 0% 155%; }\n40% { background-position: 0% 150%; }\n50% { background-position: 0% 145%; }\n70% { background-position: 0% 150%; }\n80% { background-position: 0% 155%; }\n90% { background-position: 0% 140%; }\n100% { background-position: 0% 135%; }\n}.",[1],"record .",[1],"ing .",[1],"icon { background-image: -webkit-gradient(linear, left top, left bottom, from(#f09b37), color-stop(50%, #fff)); background-image: -o-linear-gradient(top, #f09b37, #fff 50%); background-image: linear-gradient(to bottom, #f09b37, #fff 50%); background-size: 100% 200%; -webkit-animation: volatility 1.5s ease-in-out -1.5s infinite alternate; animation: volatility 1.5s ease-in-out -1.5s infinite alternate; -webkit-background-clip: text; -webkit-text-fill-color: transparent; font-size: ",[0,150],"; color: #f09b37; }\n.",[1],"record .",[1],"cancel { width: 100%; height: 30vw; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"record .",[1],"cancel .",[1],"icon { color: #fff; font-size: ",[0,150],"; }\n.",[1],"record .",[1],"tis { width: 100%; height: 10vw; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; font-size: ",[0,28],"; color: #fff; }\n.",[1],"record .",[1],"tis.",[1],"change { color: #f09b37; }\n.",[1],"content { width: 100%; }\n.",[1],"content .",[1],"msg-list { width: 96%; padding: 0 2%; position: absolute; top: 0; bottom: ",[0,100],"; }\n.",[1],"content .",[1],"msg-list .",[1],"loading { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; }\n@-webkit-keyframes stretchdelay { 0%, 40%, 100% { -webkit-transform: scaleY(0.6); transform: scaleY(0.6); }\n20% { -webkit-transform: scaleY(1); transform: scaleY(1); }\n}@keyframes stretchdelay { 0%, 40%, 100% { -webkit-transform: scaleY(0.6); transform: scaleY(0.6); }\n20% { -webkit-transform: scaleY(1); transform: scaleY(1); }\n}.",[1],"content .",[1],"msg-list .",[1],"loading .",[1],"spinner { margin: ",[0,20]," 0; width: ",[0,60],"; height: ",[0,100],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: justify; -webkit-justify-content: space-between; -ms-flex-pack: justify; justify-content: space-between; }\n.",[1],"content .",[1],"msg-list .",[1],"loading .",[1],"spinner wx-view { background-color: #f06c7a; height: ",[0,50],"; width: ",[0,6],"; border-radius: ",[0,6],"; -webkit-animation: stretchdelay 1.2s infinite ease-in-out; animation: stretchdelay 1.2s infinite ease-in-out; }\n.",[1],"content .",[1],"msg-list .",[1],"loading .",[1],"spinner .",[1],"rect2 { -webkit-animation-delay: -1.1s; animation-delay: -1.1s; }\n.",[1],"content .",[1],"msg-list .",[1],"loading .",[1],"spinner .",[1],"rect3 { -webkit-animation-delay: -1.0s; animation-delay: -1.0s; }\n.",[1],"content .",[1],"msg-list .",[1],"loading .",[1],"spinner .",[1],"rect4 { -webkit-animation-delay: -0.9s; animation-delay: -0.9s; }\n.",[1],"content .",[1],"msg-list .",[1],"loading .",[1],"spinner .",[1],"rect5 { -webkit-animation-delay: -0.8s; animation-delay: -0.8s; }\n.",[1],"content .",[1],"msg-list .",[1],"row { padding: ",[0,20]," 0; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"system { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"system wx-view { padding: 0 ",[0,30],"; height: ",[0,50],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; background-color: #c9c9c9; color: #fff; font-size: ",[0,24],"; border-radius: ",[0,40],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"system .",[1],"red-envelope wx-image { margin-right: ",[0,5],"; width: ",[0,30],"; height: ",[0,30],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right { width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble { max-width: 70%; min-height: ",[0,50],"; border-radius: ",[0,10],"  ",[0,10]," 0 ",[0,10],"; padding: ",[0,15]," ",[0,20],"; margin-left: 8px; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; font-size: ",[0,32],"; word-break: break-word; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"img, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"img { background-color: transparent; padding: 0; overflow: hidden; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"img wx-image, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"img wx-image { max-width: ",[0,350],"; max-height: ",[0,350],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"red-envelope, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"red-envelope { background-color: transparent; padding: 0; overflow: hidden; position: relative; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: start; -webkit-align-items: flex-start; -ms-flex-align: start; align-items: flex-start; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"red-envelope wx-image, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"red-envelope wx-image { width: ",[0,250],"; height: ",[0,313],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"red-envelope .",[1],"tis, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"red-envelope .",[1],"tis { position: absolute; top: 6%; font-size: ",[0,26],"; color: #9c1712; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"red-envelope .",[1],"blessing, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"red-envelope .",[1],"blessing { position: absolute; bottom: 14%; color: #e9b874; width: 80%; text-align: center; overflow: hidden; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 2; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"voice .",[1],"icon, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"voice .",[1],"icon { font-size: ",[0,40],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"voice .",[1],"icon:after, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"voice .",[1],"icon:after { content: \x22 \x22; width: ",[0,53],"; height: ",[0,53],"; border-radius: 100%; position: absolute; -webkit-box-sizing: border-box; box-sizing: border-box; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"voice .",[1],"length, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"voice .",[1],"length { font-size: ",[0,28],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"right, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"left { -webkit-flex-shrink: 0; -ms-flex-negative: 0; flex-shrink: 0; width: ",[0,80],"; height: ",[0,80],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"right wx-image, .",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"left wx-image { width: ",[0,80],"; height: ",[0,80],"; border-radius: ",[0,10],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my { width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: end; -webkit-justify-content: flex-end; -ms-flex-pack: end; justify-content: flex-end; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left { min-height: ",[0,80],"; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: end; -webkit-justify-content: flex-end; -ms-flex-pack: end; justify-content: flex-end; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble { background-color: #cce4fc; color: #222226; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"voice .",[1],"icon { color: #fff; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"voice .",[1],"length { margin-right: ",[0,20],"; }\n@-webkit-keyframes my-play { 0% { -webkit-transform: translateX(80%); transform: translateX(80%); }\n100% { -webkit-transform: translateX(0%); transform: translateX(0%); }\n}@keyframes my-play { 0% { -webkit-transform: translateX(80%); transform: translateX(80%); }\n100% { -webkit-transform: translateX(0%); transform: translateX(0%); }\n}.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"left .",[1],"bubble.",[1],"play .",[1],"icon:after { border-left: solid ",[0,10]," rgba(240, 108, 122, 0.5); -webkit-animation: my-play 1s linear infinite; animation: my-play 1s linear infinite; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"my .",[1],"right { margin-left: ",[0,15],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other { width: 100%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"left { margin-right: ",[0,15],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right { -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"username { width: 100%; height: ",[0,45],"; font-size: ",[0,24],"; color: #999; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"username .",[1],"name { margin-right: ",[0,50],"; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble { background-color: #fff; color: #333; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"voice .",[1],"icon { color: #333; }\n.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"voice .",[1],"length { margin-left: ",[0,20],"; }\n@-webkit-keyframes other-play { 0% { -webkit-transform: translateX(-80%); transform: translateX(-80%); }\n100% { -webkit-transform: translateX(0%); transform: translateX(0%); }\n}@keyframes other-play { 0% { -webkit-transform: translateX(-80%); transform: translateX(-80%); }\n100% { -webkit-transform: translateX(0%); transform: translateX(0%); }\n}.",[1],"content .",[1],"msg-list .",[1],"row .",[1],"other .",[1],"right .",[1],"bubble.",[1],"play .",[1],"icon:after { border-right: solid ",[0,10]," rgba(255, 255, 255, 0.8); -webkit-animation: other-play 1s linear infinite; animation: other-play 1s linear infinite; }\n.",[1],"windows .",[1],"mask { position: fixed; top: 100%; width: 100%; height: 100%; z-index: 1000; background-color: rgba(0, 0, 0, 0.6); opacity: 0; -webkit-transition: opacity .2s ease-out; -o-transition: opacity .2s ease-out; transition: opacity .2s ease-out; }\n.",[1],"windows .",[1],"layer { position: fixed; width: 80%; height: 70%; left: 10%; z-index: 1001; border-radius: ",[0,20],"; overflow: hidden; top: 100%; -webkit-transform: scale3d(0.5, 0.5, 1); transform: scale3d(0.5, 0.5, 1); -webkit-transition: all .2s ease-out; -o-transition: all .2s ease-out; transition: all .2s ease-out; }\n.",[1],"windows.",[1],"show { display: block; }\n.",[1],"windows.",[1],"show .",[1],"mask { top: 0; opacity: 1; }\n.",[1],"windows.",[1],"show .",[1],"layer { -webkit-transform: translate3d(0, -85vh, 0) scale3d(1, 1, 1); transform: translate3d(0, -85vh, 0) scale3d(1, 1, 1); }\n.",[1],"windows.",[1],"hide { display: block; }\n.",[1],"windows.",[1],"hide .",[1],"mask { top: 0; opacity: 0; }\n.",[1],"open-redenvelope { width: 100%; height: 70vh; background-color: #cf3c35; position: relative; }\n.",[1],"open-redenvelope .",[1],"top { width: 100%; background-color: #fe5454; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; border-radius: 0 0 100% 100%; -webkit-box-shadow: inset 0 ",[0,-20]," 0 #9c1712; box-shadow: inset 0 ",[0,-20]," 0 #9c1712; margin-bottom: ",[0,65],"; }\n.",[1],"open-redenvelope .",[1],"top .",[1],"close-btn { width: 100%; height: ",[0,80],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: end; -webkit-justify-content: flex-end; -ms-flex-pack: end; justify-content: flex-end; margin-bottom: ",[0,30],"; }\n.",[1],"open-redenvelope .",[1],"top .",[1],"close-btn .",[1],"icon { color: #9c1712; margin-top: ",[0,10],"; margin-right: ",[0,10],"; }\n.",[1],"open-redenvelope .",[1],"top wx-image { width: ",[0,130],"; height: ",[0,130],"; border: solid ",[0,12]," #cf3c35; border-radius: 100%; margin-bottom: ",[0,-65],"; }\n.",[1],"open-redenvelope .",[1],"from, .",[1],"open-redenvelope .",[1],"blessing, .",[1],"open-redenvelope .",[1],"money, .",[1],"open-redenvelope .",[1],"showDetails { width: 90%; padding: ",[0,5]," 5%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; font-size: ",[0,32],"; color: #fff; }\n.",[1],"open-redenvelope .",[1],"money { font-size: ",[0,100],"; color: #f8d757; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; padding-top: ",[0,20],"; }\n.",[1],"open-redenvelope .",[1],"showDetails { position: absolute; bottom: ",[0,20],"; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; font-size: ",[0,28],"; color: #f8d757; }\n.",[1],"open-redenvelope .",[1],"showDetails .",[1],"icon { font-size: ",[0,26],"; color: #f8d757; }\n::-webkit-scrollbar { display: none; background-color: #aaa; }\n.",[1],"my .",[1],"left .",[1],"bubble { margin-right: ",[0,15],"; position: relative; }\n.",[1],"rowss { width: 0; height: 0; font-size: 0; border: solid 4px #000; border-color: #cce4fc #cce4fc #e5e5e5 #e5e5e5; position: absolute; bottom: -8px; right: 0px; }\n.",[1],"cotent_text { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; width: 100%; }\n.",[1],"position_bubble { position: relative; }\n.",[1],"background_active { max-width: 70%; min-width: 70%; }\n.",[1],"content_text_position { position: absolute; width: 0; height: 0; font-size: 0; border: solid 8px #000; border-color: #e5e5e5 #fff #e5e5e5 #e5e5e5; left: -16px; top: 20px; }\n.",[1],"problem_list { width: 100%; background: #fff; line-height: ",[0,35],"; color: #070000; border: ",[0,1]," solid #ccc; border-radius: ",[0,20],"; margin-top: ",[0,20],"; }\n.",[1],"problem_list .",[1],"title { font-size: ",[0,32],"; }\n.",[1],"problem_list_item { width: 91%; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; line-height: ",[0,60],"; padding: 0 ",[0,8],"; border-bottom: ",[0,1]," solid #e5e5e5; padding: ",[0,15]," ",[0,20],"; font-size: ",[0,28],"; }\n.",[1],"problem_list_item .",[1],"item_img { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; width: ",[0,50],"; height: ",[0,50],"; }\n.",[1],"problem_list_item .",[1],"item_img wx-image { width: ",[0,50],"; height: ",[0,50],"; margin-right: ",[0,5],"; }\n.",[1],"problem_list_item_img { width: ",[0,50],"; height: ",[0,50],"; }\n.",[1],"problem_list_item_img wx-image { width: ",[0,50],"; height: ",[0,50],"; margin-right: ",[0,5],"; }\n.",[1],"tuchu { padding-bottom: 0; padding-bottom: constant(safe-area-inset-bottom) !important; padding-bottom: env(safe-area-inset-bottom) !important; }\n.",[1],"problem_bound { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; }\n.",[1],"problem_bound_list { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; -webkit-flex-wrap: wrap; -ms-flex-wrap: wrap; flex-wrap: wrap; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"problem_bound_item { display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; background-color: #456ffa; color: #fff; font-size: ",[0,30],"; margin: ",[0,20]," ",[0,30],"; padding: ",[0,15]," ",[0,20],"; border-radius: ",[0,10],"; }\n",],"Some selectors are not allowed in component wxss, including tag name selectors, ID selectors, and attribute selectors.(./pages/HM-chat/HM-chat.wxss:941:1)",{path:"./pages/HM-chat/HM-chat.wxss"});    
__wxAppCode__['pages/HM-chat/HM-chat.wxml']=$gwx('./pages/HM-chat/HM-chat.wxml');

__wxAppCode__['pages/home/home.wxss']=setCssToHead(["body { min-height: 100%; }\nbody{ min-height: 100%; }\nbody{ height: 100%; }\n.",[1],"content{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; background-color: #f6f6f6; min-height: 100%; height: 100%; }\n.",[1],"content_header { height: 100%; min-height: 100%; }\n.",[1],"main{ padding-bottom: 20px; }\n.",[1],"foot{ position: fixed; bottom: 0; width: 100%; margin-top: -20px; height: 20px; background-color: #eee; z-index: 9999; display: -webkit-inline-box; display: -webkit-inline-flex; display: -ms-inline-flexbox; display: inline-flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; color: #C8C7CC; font-size: ",[0,22],"; }\n.",[1],"swiper-item{ background-color: #007AFF; width: 100%; height: 100%; }\n.",[1],"swiper-item wx-image{ width: 100%; height: 100%; }\n.",[1],"middle{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; font-size: ",[0,23],"; line-height: 2; background-color: #FFFFFF; padding: ",[0,5]," 0; }\n.",[1],"middle .",[1],"number{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; margin: 0 ",[0,10],"; }\n.",[1],"middle .",[1],"number .",[1],"number_item{ height: ",[0,35],"; width: ",[0,35],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; border:",[0,1]," solid #E5E5E5; margin: 0 ",[0,6],"; color: #007AFF; }\n.",[1],"bottom{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; }\n.",[1],"bottom .",[1],"title{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; font-size: ",[0,30],"; line-height: 2.0; }\n.",[1],"bottom .",[1],"bottom_lists{ background-color: #FFFFFF; font-size: ",[0,24],"; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; width: 100%; -webkit-flex-wrap:wrap; -ms-flex-wrap:wrap; flex-wrap:wrap; }\n.",[1],"bottom_lists_item{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; width: 33.33%; -webkit-box-sizing: border-box; box-sizing: border-box; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; border-right: ",[0,1]," solid #E5E5E5; border-bottom: ",[0,1]," solid #E5E5E5; }\n.",[1],"bottom_lists_item:hover{ background-color: #E5E5E5; }\n.",[1],"bottom_lists_item wx-image{ margin: ",[0,35]," 0 ",[0,15],"; width: ",[0,75],"; height: ",[0,56],"; }\n.",[1],"bottom_lists_item .",[1],"title{ padding-bottom: ",[0,15],"; font-size: ",[0,20],"; color: #666867; }\n.",[1],"about{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; background-color: #FFFFFF; margin: 10px 0; }\n.",[1],"about .",[1],"about_us{ -webkit-box-flex: 1; -webkit-flex: 1; -ms-flex: 1; flex: 1; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; border-right: ",[0,1]," solid #E5E5E5; padding: ",[0,30]," 0; }\n.",[1],"about_us .",[1],"left wx-image{ width: ",[0,64],"; height: ",[0,48],"; margin-right: ",[0,15],"; }\n.",[1],"about_us .",[1],"right{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; }\n.",[1],"about_us .",[1],"right .",[1],"right_top{ font-size: ",[0,28],"; margin-bottom: ",[0,10],"; }\n.",[1],"about_us .",[1],"right .",[1],"right_bottom{ font-size: ",[0,24],"; color: #CCCCCC; }\n.",[1],"about .",[1],"about_rabbit{ -webkit-box-flex: 1; -webkit-flex: 1; -ms-flex: 1; flex: 1; display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; border-right: ",[0,1]," solid #E5E5E5; padding: ",[0,30]," 0; }\n.",[1],"about_rabbit .",[1],"right{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical; -webkit-box-direction: normal; -webkit-flex-direction: column; -ms-flex-direction: column; flex-direction: column; }\n.",[1],"about_rabbit .",[1],"left wx-image{ width: ",[0,64],"; height: ",[0,48],"; margin-right: ",[0,15],"; }\n.",[1],"about_rabbit .",[1],"right .",[1],"right_top{ font-size: ",[0,28],"; margin-bottom: ",[0,10],"; }\n.",[1],"about_rabbit .",[1],"right .",[1],"right_bottom{ font-size: ",[0,24],"; color: #CCCCCC; }\n.",[1],"about_logo{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-orient: horizontal; -webkit-box-direction: normal; -webkit-flex-direction: row; -ms-flex-direction: row; flex-direction: row; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; padding: ",[0,20]," 0; background-color: #FFFFFF; margin-bottom: ",[0,10],"; }\n.",[1],"about_logo .",[1],"logo{ display: -webkit-inline-box; display: -webkit-inline-flex; display: -ms-inline-flexbox; display: inline-flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; }\n.",[1],"about_logo .",[1],"logo wx-image{ width: ",[0,70],"; height: ",[0,70],"; margin-right: ",[0,15],"; }\n.",[1],"about_right_top{ font-size: ",[0,30],"; margin-bottom: ",[0,10],"; }\n.",[1],"about_right_bottom{ font-size: ",[0,24],"; color: #C8C7CC; }\n.",[1],"stand{ display: -webkit-box; display: -webkit-flex; display: -ms-flexbox; display: flex; -webkit-box-pack: center; -webkit-justify-content: center; -ms-flex-pack: center; justify-content: center; -webkit-box-align: center; -webkit-align-items: center; -ms-flex-align: center; align-items: center; color: #C8C7CC; font-size: ",[0,22],"; padding: ",[0,10]," 0 ",[0,15],"; }\n",],undefined,{path:"./pages/home/home.wxss"});    
__wxAppCode__['pages/home/home.wxml']=$gwx('./pages/home/home.wxml');

;var __pageFrameEndTime__ = Date.now();
(function() {
       plus.webview.getLaunchWebview().evalJS('__uniAppViewReadyCallback__("' + plus.webview.currentWebview()
           .id + '")')
})();

